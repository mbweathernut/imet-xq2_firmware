#ifndef __PERIPHERALS_C
#define __PERIPHERALS_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Iridium Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   peripherals.c
//
//      CONTENTS    :   Port Configuration and LED routines
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

#define  BLU_SLOW_TICS    500  // 1 sec
#define  BLU_FAST_TICS    100  // 0.2 sec

#define  PWR_100MS_TIC    100
#define  PWR_ON_TICS      10   // 1 sec
#define  PWR_OFF_TICS     50   // 5 sec

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
typedef struct
{
  FlagStatus Flag_Active;
  uint16_t TicValue;
} sWAIT_CHANNEL;

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
uint16_t Wait_Tics = 0;

// **************************************************************************

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitPSS(void);
static void InitWaitChannels(void);
static void InitWaitTimer(void);
static void InitUserTimer(void);
static void InitPushbuttonGPIO(void);
static void InitPwrCtrlGPIO(void);
static void InitLEDsGPIO(void);
static void InitI2C(void);

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
uint16_t LED_Counter = 0;
sWAIT_CHANNEL WaitChannels[WAIT_CH_CNT];

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitPSS
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the power supply supervisor
//
//  UPDATED   : 2015-06-03 JHM
//
// **************************************************************************
static void InitPSS(void)
{
  // Set the PVD to
  // Rising = 2.76V to 3.00V
  // Falling = 2.66V to 2.90V
  PWR_PVDLevelConfig(PWR_PVDLevel_7);
  PWR_PVDCmd(ENABLE);

  // Wait until voltage is above the threshold level
  while (PWR_GetFlagStatus(PWR_FLAG_PVDO) == SET);
} // InitPSS


// **************************************************************************
//
//  FUNCTION  : InitWaitChannels
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the local wait timer structures.
//
//  UPDATED   : 2016-04-12 JHM
//
// **************************************************************************
static void InitWaitChannels(void)
{
  int i;

  for (i = 0; i < WAIT_CH_CNT; i++)
  {
    WaitChannels[i].Flag_Active = RESET;
    WaitChannels[i].TicValue = 0;
  }
} // InitWaitChannels

// **************************************************************************
//
//  FUNCTION  : InitWaitTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the wait timer for precise time delays.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void InitWaitTimer(void)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(WAIT_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = WAIT_TIM_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // Configure timer clock for 10 kHz with 1 kHz tic
  Prescaler = (uint16_t)(SystemCoreClock / WAIT_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 100 - 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(WAIT_TIM, &TIM_TimeBaseStructure);

  // Enable the overflow interrupt
  TIM_ITConfig(WAIT_TIM, TIM_IT_Update, ENABLE);

  // External timer is on and constantly running
  TIM_Cmd(WAIT_TIM, ENABLE);
} // InitWaitTimer

// **************************************************************************
//
//  FUNCTION  : InitUserTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the user timer for LEDs and Pushbutton
//
//  UPDATED   : 2015-05-28 JHM
//
// **************************************************************************
static void InitUserTimer(void)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(USER_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = USER_TIM_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // Configure timer clock for 1 kHz
  Prescaler = (uint16_t)(SystemCoreClock / USER_TIM_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 0xFFFF;  // Default to maximum
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(USER_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(USER_TIM, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);

  // User timer is on and constantly running
  TIM_Cmd(USER_TIM, ENABLE);
} // InitUserTimer

// **************************************************************************
//
//  FUNCTION  : InitPushbuttonGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the pushbutton GPIO ports
//
//  UPDATED   : 2015-05-29 JHM
//
// **************************************************************************
static void InitPushbuttonGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for GPIOX
  RCC_AHBPeriphClockCmd(PB_IN_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_InitStructure.GPIO_Pin = PB_IN_PIN;
  GPIO_Init(PB_IN_PORT, &GPIO_InitStructure);
} // InitPushbutton

// **************************************************************************
//
//  FUNCTION  : InitPwrCtrlGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the output pin for the power control line
//
//  UPDATED   : 2015-05-29 JHM
//
// **************************************************************************
static void InitPwrCtrlGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for GPIOX
  RCC_AHBPeriphClockCmd(PWR_ON_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

  GPIO_InitStructure.GPIO_Pin = PWR_ON_PIN;
  GPIO_Init(PWR_ON_PORT, &GPIO_InitStructure);

  // Start with the control line low (power off)
  GPIO_ResetBits(PWR_ON_PORT, PWR_ON_PIN);
} // InitPwrCtrlGPIO

// **************************************************************************
//
//  FUNCTION  : InitLEDsCPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the LEDs
//
//  UPDATED   : 2015-04-01 JHM
//
// **************************************************************************
static void InitLEDsGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for GPIOX
  RCC_AHBPeriphClockCmd(LED_BLU_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_InitStructure.GPIO_Pin = LED_BLU_PIN;
  GPIO_Init(LED_BLU_PORT, &GPIO_InitStructure);

  // LED off
  GPIO_ResetBits(LED_BLU_PORT, LED_BLU_PIN);
} // InitLEDsGPIO

// **************************************************************************
//
//  FUNCTION  : InitI2C
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the I2C1 peripheral for sensor communications
//
//  UPDATED   : 2015-05-14 JHM
//
// **************************************************************************
static void InitI2C(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  I2C_InitTypeDef  I2C_InitStructure;

  // Select the clock to be used for I2C operation - use the SYSCLK
  RCC_I2CCLKConfig(SENS_RCC_SYSCLK);

  // Enable GPIO Peripheral clock for I2C2 and SCL and SDA)
  RCC_APB1PeriphClockCmd(SENS_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(SENS_SCL_PORT_RCC | SENS_SDA_PORT_RCC, ENABLE);

  GPIO_PinAFConfig(SENS_SCL_PORT, SENS_SCL_PINSRC, SENS_SCL_AF);
  GPIO_PinAFConfig(SENS_SDA_PORT, SENS_SDA_PINSRC, SENS_SDA_AF);

  // Configure the I2C SDA and SCL pins
  GPIO_InitStructure.GPIO_Pin = SENS_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(SENS_SCL_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = SENS_SDA_PIN;
  GPIO_Init(SENS_SCL_PORT, &GPIO_InitStructure);

  // Configuration of I2C
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  // Use I2C_Timing_Configuration_V1.0.1.xls to obtain the value for the I2C_Timing element
  I2C_InitStructure.I2C_Timing = 0x10805E89; // 100 kHz
  I2C_Init(SENS_I2C, &I2C_InitStructure);

  // Enable I2C
  I2C_Cmd(SENS_I2C, ENABLE);
} // InitI2C

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitPeripherals
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the LED and Port Configuration Peripherals
//
//  UPDATED   : 2014-11-10 JHM
//
// **************************************************************************
void InitPeripherals(void)
{
  // Initialize global variables
  Flag_DisableWWDG = RESET;
  Flag_DRR_Disable = RESET;
  LED_State = LED_OFF;

  // Initialize the power control
  InitPSS();

  // Initialize User Timer
  InitUserTimer();

  // Initialize the GPIO pin
  InitPushbuttonGPIO();

  // Initialize the power control line
  InitPwrCtrlGPIO();

  // Initialize the LED
  InitLEDsGPIO();

  // Initialize the wait channels
  InitWaitChannels();

  // Initialize the wait timer
  InitWaitTimer();

  // Initialize the sensor I2C channel
  InitI2C();
} // InitPeripherals

// **************************************************************************
//
//  FUNCTION  : Periph_I2C_FaultHandler
//
//  I/P       : I2C_TypeDef* I2Cx = Pointer to I2C peripheral structure
//
//  O/P       : None.
//
//  OPERATION : Resets the I2C bus to a known state after a fault has occurred.
//
//  UPDATED   : 2016-04-26 JHM
//
// **************************************************************************
void Periph_I2C_FaultHandler(I2C_TypeDef* I2Cx)
{
  // Turn the peripheral off
  I2C_Cmd(I2Cx, DISABLE);

  // Wait 3 clock cycles (dummy command)
  I2Cx = I2Cx;

  // Reset all values
  I2C_DeInit(I2Cx);

  // Initialize the I2C (again)
  InitI2C();
} // Periph_I2C_FaultHandler

// **************************************************************************
//
//  FUNCTION  : Periph_InitWatchdog
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the watchdog timer in window mode. The watchdog
//              value can be refreshed at any time because the window value
//              is set to the maximum value.
//
//  UPDATED   : 2015-08-10 JHM
//
// **************************************************************************
void Periph_InitWatchdog(void)
{
  // Enable the watchdog clock
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

  // Configure watchdog
  WWDG_SetPrescaler(WWDG_Prescaler_8);
  WWDG_SetWindowValue(WWDG_MAX_CNT);

  // Start the timer
  WWDG_Enable(WWDG_MAX_CNT);
} // Periph_InitWatchdog

// **************************************************************************
//
//  FUNCTION  : Pushbutton_Init
//
//  I/P       : sPushbutton* Button - Pointer to a pushbutton structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the pushbutton
//
//  UPDATED   : 2015-05-29 JHM
//
// **************************************************************************
void Pushbutton_Init(sPushbutton* Button)
{
  uint16_t CounterValue;

  // Initialize the button structure
  Button->TicsCounter = 0;
  Button->Flag_Startup = RESET;
  Button->Flag_Shutdown = RESET;

  // Read the initial state
  if (GPIO_ReadInputDataBit(PB_IN_PORT, PB_IN_PIN) == Bit_RESET)
  {
    // Power applied via battery
    Button->State = PB_ST_INIT;
    Button->Flag_ButtonPressed = SET;
  }
  else
  {
    // Power applied via USB
    Button->State = PB_ST_BYPASS;
    Button->Flag_ButtonPressed = RESET;
    SetLEDState(LED_BLINK);
  }

  // Get counter value (clock is always running)
  CounterValue = TIM_GetCounter(USER_TIM);
  // Set the CCR Value for 100 ms
  TIM_SetCompare1(USER_TIM, CounterValue + PWR_100MS_TIC);
  // Enable the 100 ms tic interrupt
  TIM_ITConfig(USER_TIM, TIM_IT_CC1, ENABLE);
} // Pushbutton_Init

// **************************************************************************
//
//  FUNCTION  : Pushbutton_Handler
//
//  I/P       : sPushbutton* Button - Pointer to a pushbutton structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the pushbutton
//
//  UPDATED   : 2015-05-29 JHM
//
// **************************************************************************
void Pushbutton_Handler(sPushbutton* Button)
{
  switch(Button->State)
  {
    case PB_ST_INIT:
      // Check if the button has been pressed long enough
      if (PWR_ON_TICS < Button->TicsCounter)
      {
        // Set the control line high
        GPIO_SetBits(PWR_ON_PORT, PWR_ON_PIN);
        Button->Flag_Startup = SET;
        SetLEDState(LED_ON);
      }
      // Make sure the user has released the button before advancing states
      if ((Button->Flag_Startup == SET) && (Button->Flag_ButtonPressed == RESET))
      {
        Button->TicsCounter = 0;
        Button->State = PB_ST_ON;
        SetLEDState(LED_BLINK);
      }
      break;

    case PB_ST_ON:
      if ((PWR_OFF_TICS < Button->TicsCounter))
      {
        Button->Flag_Shutdown = SET;
        // Advance the state machine
        Button->State = PB_ST_SHUTDOWN;
        // Set control line low to turn off power. This has no effect if
        // the board is connected to USB
        GPIO_ResetBits(PWR_ON_PORT, PWR_ON_PIN);
        // Turn LED off
        SetLEDState(LED_OFF);
      }
      break;

    case PB_ST_SHUTDOWN:
      break;

    case PB_ST_BYPASS:
      // Do nothing.  Pushbutton is disabled because power was applied via USB
      break;
  } //switch
} // Pushbutton_Handler

// **************************************************************************
//
//  FUNCTION    :  Wait
//
//  I/P         :  uint8_t Channel - 1, 2, 3, 4
//                 uint16_t ms - time to wait in milliseconds
//
//  O/P         :  None.
//
//  OPERATION   :  Sets the global variable ChannelX_Delay_Flag for the
//                 duration of time specified in milliseconds.  Once the time
//                 has expired it resets the flag.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
void Wait(uint8_t Channel, uint16_t ms)
{
  WaitChannels[Channel].TicValue = Wait_Tics + ms;
  WaitChannels[Channel].Flag_Active = SET;
} // Wait

// **************************************************************************
//
//  FUNCTION    :  GetWaitFlagStatus
//
//  I/P         :  uint8_t Channel - 1, 2, 3, 4
//
//  O/P         :  FlagStatus - SET (waiting) RESET (idle)
//
//  OPERATION   :  Returns the flag status of the wait channel selected.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
FlagStatus GetWaitFlagStatus(uint8_t Channel)
{
  return WaitChannels[Channel].Flag_Active;
} // GetWaitFlagStatus

// **************************************************************************
//
//  FUNCTION    :  WaitHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Handles the 1ms wait timer interrupt
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void WaitHandler(void)
{
  int i;

  for (i = 0; i < WAIT_CH_CNT; i++)
  {
    if (WaitChannels[i].Flag_Active == SET)
    {
      if (WaitChannels[i].TicValue == Wait_Tics)
      {
        WaitChannels[i].Flag_Active = RESET;
      }
    }
  } // for
} // WaitHandler

// **************************************************************************
//
//  FUNCTION    :  SetLEDState
//
//  I/P         :  uint8_t NewState
//
//  O/P         :  None.
//
//  OPERATION   :  Modifies the state of the LED
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void SetLEDState(Type_LED_State State)
{
  // Update the global variable
  LED_State = State;

  // Reset the counter
  LED_Counter = 0;

  if (LED_State == LED_OFF)
  {
    GPIO_ResetBits(LED_BLU_PORT, LED_BLU_PIN);
  }
  else
  {
    GPIO_SetBits(LED_BLU_PORT, LED_BLU_PIN);
  }
} // SetLEDState

// **************************************************************************
//
//  FUNCTION    :  LED_Handler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Handles the LED
//
//  UPDATED     :  2016-11-29 JHM
//
// **************************************************************************
void LED_Handler(void)
{
  if (GetWaitFlagStatus(WAIT_LED_CH) == SET)
  {
    return;
  }

  switch (LED_State)
  {
  case LED_BLINK:
    // Toggle the LED
    LED_BLU_PORT->ODR ^= LED_BLU_PIN;
    // Wait for 1 second
    Wait(WAIT_LED_CH, 1000);
    break;

  case LED_FAST_BLINK:
    // Toggle the LED
    LED_BLU_PORT->ODR ^= LED_BLU_PIN;
    // Wait for 100 ms
    Wait(WAIT_LED_CH, 100);
    break;

  default:
    // Do nothing. The LED is forced on or off
    break;
  }
} // LED_Handler

// **************************************************************************
//
//        INTERRUPTS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION    :  TIM13_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for the wait routine.
//
//  UPDATED     :  2016-11-29 JHM
//
// **************************************************************************
void TIM13_IRQHandler(void)
{
  if (TIM_GetITStatus(WAIT_TIM, TIM_IT_Update) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(WAIT_TIM, TIM_FLAG_Update);

    // Increment the wait tics
    Wait_Tics++;

    // From peripherals.c
    WaitHandler();
  }
} // TIM13_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM4_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void TIM4_IRQHandler(void)
{
  uint16_t CounterValue;

  // CCR1 Interrupt - Pushbutton
  if (TIM_GetITStatus(USER_TIM, TIM_IT_CC1) == SET)
  {
    // Clear the interrupt
    TIM_ClearITPendingBit(USER_TIM, TIM_IT_CC1);

    // Get counter value (clock is always running)
    CounterValue = TIM_GetCounter(USER_TIM);

    // Start the next 100ms tic
    TIM_SetCompare1(USER_TIM, CounterValue + PWR_100MS_TIC);

    // Increment the TicsCounter if the port is low (button is pushed)
    if (GPIO_ReadInputDataBit(PB_IN_PORT, PB_IN_PIN) == Bit_RESET)
    {
      Pushbutton.Flag_ButtonPressed = SET;
      Pushbutton.TicsCounter++;
    }
    else
    {
      Pushbutton.Flag_ButtonPressed = RESET;
      if (Pushbutton.TicsCounter > 0)
      {
        Pushbutton.TicsCounter--;
      }
    }
  } // if
} // TIM4_IRQHandler

#endif
