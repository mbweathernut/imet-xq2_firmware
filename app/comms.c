#ifndef __COMMSRSB_C
#define __COMMSRSB_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Iridium Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   comms.c
//
//      CONTENTS    :   File for communication through the serial port
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//        CONSTANTS
// **************************************************************************
// None.

// *************************************************************************
//        TYPES
// *************************************************************************
#define  CMD_TYPE_NULL           0
#define  CMD_TYPE_GET            1
#define  CMD_TYPE_SET            2

// Error Messages
#define  ERR_COMMAND             "ERR01\r\n"
#define  ERR_TYPE                "ERR02\r\n"
#define  ERR_PARAM               "ERR03\r\n"
#define  ERR_FLASH               "ERR04\r\n"

#define  MAX_MSG_SIZE            30

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitUSART(void);
static void TransmissionHandler(void);
static void ReceptionHandler(void);
static void ProcessMessage(void);

// Message handlers
static void RST_Handler(void);
static void STP_Handler(void);
static void STA_Handler(void);
static void ERA_Handler(void);
static void FMO_Handler(void);
static void RDA_Handler(void);
static void RDB_Handler(void);
static void MEM_Handler(void);
static void CNT_Handler(void);
static void SAV_Handler(void);
static void SRN_Handler(void);
static void DRR_Handler(void);
static void STX_Handler(uint8_t CoefficientNumber);
static void CPB_Handler(void);
static void CTB_Handler(void);
//static void CHB_Handler(void);

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
char *ptrMessage;
uint8_t CommandType;
char Message[MAX_MSG_SIZE];

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : InitUSART
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes User Interface Serial Port
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
static void InitUSART(void)
{
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(COMMS_PORT_RCC, ENABLE);
  RCC_APB2PeriphClockCmd(COMMS_USART_RCC, ENABLE);

  // Configure Pins
  GPIO_InitStructure.GPIO_Pin = COMMS_TX_PIN | COMMS_RX_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

  GPIO_Init(COMMS_PORT, &GPIO_InitStructure);

  // Set pins to alternate functions
  GPIO_PinAFConfig(COMMS_PORT, COMMS_TX_PIN_SRC, COMMS_TX_AF);
  GPIO_PinAFConfig(COMMS_PORT, COMMS_RX_PIN_SRC, COMMS_RX_AF);

  // Configure Serial Hardware
  USART_InitStructure.USART_BaudRate = COMMS_USART_BAUD;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

  USART_Init(COMMS_USART, &USART_InitStructure);

  // Initialize Interrupts
  NVIC_InitStructure.NVIC_IRQChannel = COMMS_USART_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);

  // Disable transmit interrupt (we will enable prior to transmission)
  USART_ITConfig(COMMS_USART, USART_IT_TXE, DISABLE);
  // Wait for transmission to become available
  while (USART_GetFlagStatus(COMMS_USART, USART_FLAG_TC) == RESET);

  // Enable receive interrupt
  USART_ITConfig(COMMS_USART, USART_IT_RXNE, ENABLE);
  // Wait for receiver to become available
  while (USART_GetFlagStatus(COMMS_USART, USART_IT_RXNE) != RESET);

  // Enable USART
  USART_Cmd(COMMS_USART, ENABLE);
} // InitUSART

// **************************************************************************
//
//  FUNCTION  : TransmissionHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the transmission interrupt
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
static void TransmissionHandler(void)
{
  // Send the data
  USART_SendData(COMMS_USART, Comms_Tx.TxBuffer[Comms_Tx.Start]);
  // Wait for the transmission to end
  while (USART_GetFlagStatus(COMMS_USART, USART_FLAG_TXE == RESET));

  // Increment the start index (safe)
  Comms_Tx.Start = (Comms_Tx.Start + 1) % COMMS_TX_BUF_SIZE;

  // End the transmission if the start and end index are the same
  if (Comms_Tx.Start == Comms_Tx.End)
  {
    // Disable the interrupt
    USART_ITConfig(COMMS_USART, USART_IT_TXE, DISABLE);
  }
} // TransmissionHandler

// **************************************************************************
//
//  FUNCTION  : ReceptionHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the receive interrupt
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
static void ReceptionHandler(void)
{
  // Move the received data into the buffer
  Comms_Rx.RxBuffer[Comms_Rx.End] = USART_ReceiveData(COMMS_USART);
  // Clear RXNE flag to prevent overflow error
  USART_ClearFlag(COMMS_USART, USART_FLAG_RXNE);

  if (Comms_Rx.RxBuffer[Comms_Rx.End] == '\n')
  {
    Comms_Rx.MsgAvailable++;
    // Increment the end index (safe)
    Comms_Rx.End = (Comms_Rx.End + 1) % COMMS_RX_BUF_SIZE;
    // Terminate with null
    Comms_Rx.RxBuffer[Comms_Rx.End] = 0;
  }
  // Increment the end index (safe)
  Comms_Rx.End = (Comms_Rx.End + 1) % COMMS_RX_BUF_SIZE;
} // ReceptionHandler

// **************************************************************************
//
//  FUNCTION  : GetMessage
//
//  I/P       : char* message - Pointer to destination array
//
//  O/P       : uint8_t - length of the message
//
//  OPERATION : Copies the message from the recieve buffer to the destination
//              array.  Returns 0 if no message is available or the length of
//              the message if it is available.
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
static void ProcessMessage(void)
{
  int i;

  if ((Message[0] = 0) || (Comms_Rx.Start == Comms_Rx.End))
  {
    Comms_Rx.MsgAvailable = 0;
    return;
  }

  for (i = 0; i < MAX_MSG_SIZE; i++)
  {
    // Read from the buffer to the message array
    Message[i] = Comms_Rx.RxBuffer[Comms_Rx.Start];
    // Increment buffer (safe)
    Comms_Rx.Start = (Comms_Rx.Start + 1) % COMMS_RX_BUF_SIZE;

    if (Message[i] == 0)
    {
      break;
    }
  }

  if (i == MAX_MSG_SIZE)
  {
    Message[i - 1] = 0;
  }

  // Decrement message available
  Comms_Rx.MsgAvailable--;
} // GetMessage

// **************************************************************************
//
//  FUNCTION  : RST_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Immediately resets the iMet-XQ using the watchdog timer
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void RST_Handler(void)
{
  switch (CommandType)
  {
    case CMD_TYPE_NULL:
#ifndef WATCHDOG
      // Enable the watchdog clock
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
      // Start the timer
      WWDG_Enable(WWDG_MAX_CNT);
#endif
      // Set watchdog timer to a value that will immediately cause reset
      WWDG_SetCounter(0x3F);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // RST_Hander

// **************************************************************************
//
//  FUNCTION  : STP_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Stops the DRR timer to disable memory/serial data output.
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void STP_Handler(void)
{
  switch (CommandType)
  {
    case CMD_TYPE_NULL:
      // Set the flag to disable data reporting
      Flag_DRR_Disable = SET;
      Comms_TransmitMessage("OK\r\n");
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // STA_Hander

// **************************************************************************
//
//  FUNCTION  : STA_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Starts the DRR timer for memory/serial data if it has been disabled.
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void STA_Handler(void)
{
  switch (CommandType)
  {
    case CMD_TYPE_NULL:
      // Reset the flag that disables data reporting
      Flag_DRR_Disable = RESET;
      Comms_TransmitMessage("OK\r\n");
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // STA_Hander

// **************************************************************************
//
//  FUNCTION  : ERA_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Erases the flash memory. This takes approximately 12 seconds
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void ERA_Handler(void)
{
  switch (CommandType)
  {
    case CMD_TYPE_NULL:
      // Erase the memory
      Comms_TransmitMessage("Starting to erase...");
      // Erase the memory
      Spansion_ChipErase(&Spansion);
      // Notify end of erase
      Comms_TransmitMessage("OK\r\n");
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // ERA_Hander

// **************************************************************************
//
//  FUNCTION  : FMO_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Disables writing to flash memory.
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void FMO_Handler(void)
{
  int newValue;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(strVariable, FlashMode, 1);
      Message[0] = 0;
      strcat(Message, "FMO=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");
      newValue = ReadInt16(ptrMessage);

      if (newValue == FLASH_MODE_DISABLE)
      {
        FlashMode = FLASH_MODE_DISABLE;
        iMetXQ_Config.FlashMode = FLASH_MODE_DISABLE;
        Comms_TransmitMessage("FMO=0\r\n");
      }
      else if (newValue == FLASH_MODE_GPS)
      {
        FlashMode = FLASH_MODE_GPS;
        iMetXQ_Config.FlashMode = FLASH_MODE_GPS;
        Comms_TransmitMessage("FMO=1\r\n");
      }
      else if (newValue == FLASH_MODE_ON)
      {
        FlashMode = FLASH_MODE_ON;
        iMetXQ_Config.FlashMode = FLASH_MODE_ON;
        Comms_TransmitMessage("FMO=2\r\n");
      }
      else
      {
        Comms_TransmitMessage(ERR_PARAM);
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // FMO_Hander

// **************************************************************************
//
//  FUNCTION  : RDA_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Reads the flash memory and sends it as ASCII data through the
//              serial port.
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void RDA_Handler(void)
{
  char Message[100];
  char Variable[20];
  sXQ_Packet Packet;
  uint32_t ReadIndex;

  if (CommandType != CMD_TYPE_NULL)
  {
    Comms_TransmitMessage(ERR_TYPE);
    return;
  }

  ReadIndex = 0;

  while (ReadIndex < Spansion.Index)
  {
    Spansion_ReadXQPacket(&Spansion, &Packet, ReadIndex);
    ReadIndex += MEM_XQ_SIZE;

    Message[0] = 0;
    strcat(Message, "XQ,");
    // Pressure
    sprintSignedNumber(Variable, Packet.Pressure, 6);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Temperature
    sprintSignedNumber(Variable, Packet.Temperature, 4);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Humidity
    sprintSignedNumber(Variable, Packet.Humidity, 4);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Humidity Temperature
    sprintSignedNumber(Variable, Packet.HumidityTemperature, 4);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Date
    sprintUnsignedNumber(Variable, Packet.Year, 4);
    strcat(Message, Variable);
    strcat(Message, "/");
    sprintUnsignedNumber(Variable, Packet.Month, 2);
    strcat(Message, Variable);
    strcat(Message, "/");
    sprintUnsignedNumber(Variable, Packet.Day, 2);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Time
    sprintUnsignedNumber(Variable, Packet.Hour, 2);
    strcat(Message, Variable);
    strcat(Message, ":");
    sprintUnsignedNumber(Variable, Packet.Minute, 2);
    strcat(Message, Variable);
    strcat(Message, ":");
    sprintUnsignedNumber(Variable, Packet.Second, 2);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Longitude
    sprintSignedNumber(Variable, Packet.Longitude, 10);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Latitude
    sprintSignedNumber(Variable, Packet.Latitude, 10);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Altitude
    sprintSignedNumber(Variable, Packet.hMSL, 8);
    strcat(Message, Variable);
    strcat(Message, ",");
    // Satellites
    sprintUnsignedNumber(Variable, Packet.Satellites, 2);
    strcat(Message, Variable);
    strcat(Message, "\r\n");
    Comms_TransmitMessage(Message);

    Wait(1, 25);
    while (GetWaitFlagStatus(1) == SET)
    {
      // Reset the watchdog timer
      WWDG_SetCounter(WWDG_MAX_CNT);
    }
  }
} // RDA_Hander

// **************************************************************************
//
//  FUNCTION  : RDB_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Reads the flash memory and sends it as ASCII data through the
//              serial port.
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void RDB_Handler(void)
{
  sXQ_Packet Packet;
  uint8_t Bytes[MEM_XQ_SIZE + 1];
  uint8_t* Offset;
  uint32_t ReadIndex;

  // Validate command type
  if (CommandType != CMD_TYPE_NULL)
  {
    Comms_TransmitMessage(ERR_TYPE);
    return;
  }

  // Lets start at the very beginning, a very good place to start
  ReadIndex = 0;

  while (ReadIndex < Spansion.Index)
  {
    // Set the pointer to bytes array
    Offset = &Bytes[0];
    // Read the packet from memory
    Spansion_ReadXQPacket(&Spansion, &Packet, ReadIndex);
    // Increment the read index
    ReadIndex += MEM_XQ_SIZE;

    // Build the array
    // GPS Year
    memcpy(Offset, &Packet.Year, sizeof(Packet.Year));
    Offset += sizeof(Packet.Year);
    // GPS Month
    memcpy(Offset, &Packet.Month, sizeof(Packet.Month));
    Offset += sizeof(Packet.Month);
    // GPS Day
    memcpy(Offset, &Packet.Day, sizeof(Packet.Day));
    Offset += sizeof(Packet.Day);
    // GPS Hour
    memcpy(Offset, &Packet.Hour, sizeof(Packet.Hour));
    Offset += sizeof(Packet.Hour);
    // GPS Minute
    memcpy(Offset, &Packet.Minute, sizeof(Packet.Minute));
    Offset += sizeof(Packet.Minute);
    // GPS Second
    memcpy(Offset, &Packet.Second, sizeof(Packet.Second));
    Offset += sizeof(Packet.Second);
    // Pressure
    memcpy(Offset, &Packet.Pressure, sizeof(Packet.Pressure));
    Offset += sizeof(Packet.Pressure);
    // Temperature
    memcpy(Offset, &Packet.Temperature, sizeof(Packet.Temperature));
    Offset += sizeof(Packet.Temperature);
    // Humidity
    memcpy(Offset, &Packet.Humidity, sizeof(Packet.Humidity));
    Offset += sizeof(Packet.Humidity);
    // Humidity Temperature
    memcpy(Offset, &Packet.HumidityTemperature, sizeof(Packet.HumidityTemperature));
    Offset += sizeof(Packet.HumidityTemperature);
    // Longitude
    memcpy(Offset, &Packet.Longitude, sizeof(Packet.Longitude));
    Offset += sizeof(Packet.Longitude);
    // Latitude
    memcpy(Offset, &Packet.Latitude, sizeof(Packet.Latitude));
    Offset += sizeof(Packet.Latitude);
    // Altitude
    memcpy(Offset, &Packet.hMSL, sizeof(Packet.hMSL));
    Offset += sizeof(Packet.hMSL);
    // Satellites
    memcpy(Offset, &Packet.Satellites, sizeof(Packet.Satellites));
    Offset += sizeof(Packet.Satellites);
    *Offset = 0;

    // Send the data
    Comms_TransmitBinary(Bytes, sizeof(Bytes));

    // Wait for data to transmit (takes approximately 6 ms plus overhead)
    Wait(1, 7);
    while (GetWaitFlagStatus(1) == SET)
    {
      WWDG_SetCounter(WWDG_MAX_CNT);
    }
  }
} // RDB_Hander

// **************************************************************************
//
//  FUNCTION  : MEM_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Gets the percentage of memory used
//              Example: 0578 = 57.8%
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void MEM_Handler(void)
{
  uint16_t dPercent;
  float fPercent;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      fPercent = (float)Spansion.Index / (float)MEM_SIZE_BYTES * 100.0;
      dPercent = (uint16_t)(fPercent * 10.0);
      sprintUnsignedNumber(strVariable, dPercent, 4);
      Message[0] = 0;
      strcat(Message, "MEM=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // MEM_Hander

// **************************************************************************
//
//  FUNCTION  : CNT_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Sends the number of packets in memory
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void CNT_Handler(void)
{
  uint32_t Packets;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      Packets = Spansion.Index / MEM_XQ_SIZE;
      sprintUnsignedNumber(strVariable, Packets, 5);
      Message[0] = 0;
      strcat(Message, "CNT=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // CNT_Hander

// **************************************************************************
//
//  FUNCTION  : SAV_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Saves the configuration to flash memory
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
static void SAV_Handler(void)
{
  switch (CommandType)
  {
    case CMD_TYPE_NULL:
      if (Config_Save() == FLASH_COMPLETE)
      {
        Comms_TransmitMessage("OK\r\n");
      }
      else
      {
        Comms_TransmitMessage(ERR_FLASH);
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
  } // switch
} // SAV_Hander

// **************************************************************************
//
//  FUNCTION  : SRN_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the serial number command
//
//  UPDATED   : 2015-06-12 JHM
//
// **************************************************************************
static void SRN_Handler(void)
{
  uint32_t SerialNumber;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(strVariable, iMetXQ_Config.SerialNumber, 8);
      Message[0] = 0;
      strcat(Message, "SRN=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");
      SerialNumber = (uint32_t)atol(ptrMessage);

      if ((SerialNumber == 0) || (SerialNumber > 99999999))
      {
        Comms_TransmitMessage(ERR_PARAM);
      }
      else
      {
        iMetXQ_Config.SerialNumber = (uint32_t)SerialNumber;
        sprintUnsignedNumber(strVariable, iMetXQ_Config.SerialNumber, 8);
        Message[0] = 0;
        strcat(Message, "SRN=");
        strcat(Message, strVariable);
        strcat(Message, "\r\n");
        Comms_TransmitMessage(Message);
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SRN_Hander

// **************************************************************************
//
//  FUNCTION  : DRR_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the data report rate command
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void DRR_Handler(void)
{
  uint16_t DataReportRate;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(strVariable, iMetXQ_Config.DataReportRate, 4);
      Message[0] = 0;
      strcat(Message, "DRR=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");
      DataReportRate = (uint16_t)ReadInt16(ptrMessage);

      if (DataReportRate == 0)
      {
        Comms_TransmitMessage(ERR_PARAM);
      }
      else
      {
        iMetXQ_Config.DataReportRate = (uint16_t)DataReportRate;
        strcpy(Message, "DRR=");
        sprintUnsignedNumber(strVariable, DataReportRate, 4);
        strcat(Message, strVariable);
        strcat(Message, "\r\n");
        Comms_TransmitMessage(Message);
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // DRR_Hander

// **************************************************************************
//
//  FUNCTION  : STX_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the steinhart-hart coefficient read/write commands.
//
//  UPDATED   : 2015-06-19 JHM
//
// **************************************************************************
static void STX_Handler(uint8_t CoefficientNumber)
{
  float Coefficient;

  switch(CommandType)
  {
    case CMD_TYPE_SET:
      // Trim \r\n from message
      strtok(ptrMessage, "\r\n");
      Coefficient = ReadFloat(ptrMessage);

      if (CoefficientNumber == 0)
      {
        Thermistor.Coefficients.A0 = Coefficient;
        iMetXQ_Config.Coefficients.A0 = Thermistor.Coefficients.A0;
      }
      else if (CoefficientNumber == 1)
      {
        Thermistor.Coefficients.A1 = Coefficient;
        iMetXQ_Config.Coefficients.A1 = Thermistor.Coefficients.A1;
      }
      else if (CoefficientNumber == 2)
      {
        Thermistor.Coefficients.A2 = Coefficient;
        iMetXQ_Config.Coefficients.A2 = Thermistor.Coefficients.A2;
      }
      else if (CoefficientNumber == 3)
      {
        Thermistor.Coefficients.A3 = Coefficient;
        iMetXQ_Config.Coefficients.A3 = Thermistor.Coefficients.A3;
      }
      break;

    case CMD_TYPE_GET:
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      return;
  }

  // Initialize the string
  Message[0] = 0;

  switch(CoefficientNumber)
  {
    case 0:
      strcat(Message, "ST0=");
      sprintFloat(strVariable, Thermistor.Coefficients.A0, 6);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case 1:
      strcat(Message, "ST1=");
      sprintFloat(strVariable, Thermistor.Coefficients.A1, 6);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case 2:
      strcat(Message, "ST2=");
      sprintFloat(strVariable, Thermistor.Coefficients.A2, 6);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
       break;

    case 3:
      strcat(Message, "ST3=");
      sprintFloat(strVariable, Thermistor.Coefficients.A3, 6);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      return;
  } // switch (CoefficientNumber)
} // STX_Handler

// **************************************************************************
//
//  FUNCTION  : CPB_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the pressure correction bias command.
//
//  UPDATED   : 2015-07-08 JHM
//
// **************************************************************************
static void CPB_Handler(void)
{
  int16_t Bias;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintSignedNumber(strVariable, PressureSensor.BiasCorrection, 3);
      Message[0] = 0;
      strcat(Message, "CPB=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");

      if ((strcmp(ptrMessage, "0") == 0) || (strcmp(ptrMessage, "000") == 0))
      {
        Bias = 0;
      }
      else
      {
        Bias = ReadInt16(ptrMessage);
        if ((Bias == 0) || (Bias > 999) || (Bias < -999))
        {
          Comms_TransmitMessage(ERR_PARAM);
          return;
        }
      }
      // Set flash configuration
      iMetXQ_Config.PressureBias = Bias;
      // Set current pressure adjustment
      PressureSensor.BiasCorrection = Bias;
      // Build message
      Message[0] = 0;
      strcat(Message, "CPB=");
      sprintSignedNumber(strVariable, Bias, 3);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      // Send message
      Comms_TransmitMessage(Message);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // CPB_Hander

// **************************************************************************
//
//  FUNCTION  : CTB_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the correction temperature bias command.
//
//  UPDATED   : 2015-07-02 JHM
//
// **************************************************************************
static void CTB_Handler(void)
{
  int16_t Bias;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintSignedNumber(strVariable, Thermistor.Coefficients.BiasT, 3);
      Message[0] = 0;
      strcat(Message, "CTB=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");

      if ((strcmp(ptrMessage, "0") == 0) || (strcmp(ptrMessage, "000") == 0))
      {
        Bias = 0;
      }
      else
      {
        Bias = ReadInt16(ptrMessage);
        if ((Bias == 0) || (Bias > 999) || (Bias < -999))
        {
          Comms_TransmitMessage(ERR_PARAM);
          return;
        }
      }
      // Set flash configuration
      iMetXQ_Config.Coefficients.BiasT = Bias;
      // Set current temperature adjustment
      Thermistor.Coefficients.BiasT = Bias;
      // Build message
      Message[0] = 0;
      strcat(Message, "CTB=");
      sprintSignedNumber(strVariable, Bias, 3);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      // Send message
      Comms_TransmitMessage(Message);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // CTB_Hander
/*
// **************************************************************************
//
//  FUNCTION  : CHB_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the correction of humidity bias command.
//
//  UPDATED   : 2015-07-02 JHM
//
// **************************************************************************
static void CHB_Handler(void)
{
  int16_t Bias;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintSignedNumber(strVariable, HTU21D_Sensor.HumidityBias, 3);
      Message[0] = 0;
      strcat(Message, "CHB=");
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      Comms_TransmitMessage(Message);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");

      if ((strcmp(ptrMessage, "0") == 0) || (strcmp(ptrMessage, "000") == 0))
      {
        Bias = 0;
      }
      else
      {
        Bias = ReadInt16(ptrMessage);
        if ((Bias == 0) || (Bias > 999) || (Bias < -999))
        {
          Comms_TransmitMessage(ERR_PARAM);
          return;
        }
      }
      // Set flash configuration
      iMetXQ_Config.HumidityBias = Bias;
      // Set current temperature adjustment
      HTU21D_Sensor.HumidityBias = Bias;
      // Build message
      Message[0] = 0;
      strcat(Message, "CHB=");
      sprintSignedNumber(strVariable, Bias, 3);
      strcat(Message, strVariable);
      strcat(Message, "\r\n");
      // Send message
      Comms_TransmitMessage(Message);
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // CHB_Hander
*/
// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : Commms_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes external USART
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
void Comms_Init(void)
{
  int i;

  ptrMessage = NULL;
  CommandType = CMD_TYPE_NULL;

  // Initialize the private Tx data structure
  Comms_Tx.Start = 0;
  Comms_Tx.End = 0;

  for (i = 0; i < COMMS_TX_BUF_SIZE; i++)
  {
    Comms_Tx.TxBuffer[i] = 0;
  }

  // Initialize the private Rx data structure
  Comms_Rx.Start = 0;
  Comms_Rx.End = 0;
  Comms_Rx.MsgAvailable = 0;

  for (i = 0; i < COMMS_RX_BUF_SIZE; i++)
  {
    Comms_Rx.RxBuffer[i] = 0;
  }

  for (i = 0; i < MAX_MSG_SIZE; i++)
  {
    Message[i] = 0;
  }

  // Initialize the serial port
  InitUSART();
} // Comms_Init

// **************************************************************************
//
//  FUNCTION  : Comms_TransmitMessage
//
//  I/P       : char* message = Pointer to message array
//
//  O/P       : None.
//
//  OPERATION : Transmits a message via the external USART
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
void Comms_TransmitMessage(char* Message)
{
  int i;

  for (i = 0; i < strlen(Message); i++)
  {
    Comms_Tx.TxBuffer[Comms_Tx.End] = Message[i];
    Comms_Tx.End = (Comms_Tx.End + 1) % COMMS_TX_BUF_SIZE;
  }

  // Enable the TX empty interrupt (which should immediately occur)
  if (USART_GetITStatus(COMMS_USART, USART_IT_TXE) == RESET)
  {
    USART_ITConfig(COMMS_USART, USART_IT_TXE, ENABLE);
  }
} // Comms_TransmitMessage

// **************************************************************************
//
//  FUNCTION  : Comms_TransmitMessage
//
//  I/P       : uint8_t* Data = Pointer to byte array
//              uint16_t Length = Size of the message (must be less than data
//                                buffer size)
//
//  O/P       : None.
//
//  OPERATION : Transmits binary data via the external USART
//
//  UPDATED   : 2015-06-24 JHM
//
// **************************************************************************
void Comms_TransmitBinary(uint8_t* Data, uint16_t Length)
{
  int i;

  for (i = 0; (i < Length) && (i < COMMS_TX_BUF_SIZE); i++)
  {
    Comms_Tx.TxBuffer[Comms_Tx.End] = *Data;
    Data++;
    Comms_Tx.End = (Comms_Tx.End + 1) % COMMS_TX_BUF_SIZE;
  }

  // Enable the TX empty interrupt (which should immediately occur)
  if (USART_GetITStatus(COMMS_USART, USART_IT_TXE) == RESET)
  {
    USART_ITConfig(COMMS_USART, USART_IT_TXE, ENABLE);
  }
} // Comms_TransmitBinary

// **************************************************************************
//
//  FUNCTION  : Comms_MessageHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles a message if available in the queue.
//
//  UPDATED   : 2015-05-20 JHM
//
// **************************************************************************
void Comms_MessageHandler(void)
{

  // Exit if there are no messages available
  if (Comms_Rx.MsgAvailable == 0)
  {
    return;
  }

  // Process the message
  ProcessMessage();

  // Convert to upper case
  strupr(Message);

  if (strchr(Message, '?') != NULL)
  {
    CommandType = CMD_TYPE_GET;
  }
  else if (strchr(Message, '=') != NULL)
  {
    CommandType = CMD_TYPE_SET;
    ptrMessage = strchr(Message, '=');
    ptrMessage++;
  }
  else
  {
    CommandType = CMD_TYPE_NULL;
  }

  // Remove the \r\n
  strtok(Message, "\r?=");

  if (strcmp(Message, "/RST") == 0){ RST_Handler(); }
  else if (strcmp(Message, "/STP") == 0){ STP_Handler(); }
  else if (strcmp(Message, "/STA") == 0){ STA_Handler(); }
  else if (strcmp(Message, "/ERA") == 0){ ERA_Handler(); }
  else if (strcmp(Message, "/FMO") == 0){ FMO_Handler(); }
  else if (strcmp(Message, "/RDA") == 0){ RDA_Handler(); }
  else if (strcmp(Message, "/RDB") == 0){ RDB_Handler(); }
  else if (strcmp(Message, "/MEM") == 0){ MEM_Handler(); }
  else if (strcmp(Message, "/CNT") == 0){ CNT_Handler(); }
  else if (strcmp(Message, "/SAV") == 0){ SAV_Handler(); }
  else if (strcmp(Message, "/SRN") == 0){ SRN_Handler(); }
  else if (strcmp(Message, "/DRR") == 0){ DRR_Handler(); }
  else if (strcmp(Message, "/ST0") == 0){ STX_Handler(0); }
  else if (strcmp(Message, "/ST1") == 0){ STX_Handler(1); }
  else if (strcmp(Message, "/ST2") == 0){ STX_Handler(2); }
  else if (strcmp(Message, "/ST3") == 0){ STX_Handler(3); }
  else if (strcmp(Message, "/CPB") == 0){ CPB_Handler(); }
  else if (strcmp(Message, "/CTB") == 0){ CTB_Handler(); }
  //else if (strcmp(Message, "/CHB") == 0){ CHB_Handler(); }
  else
  {
    Comms_TransmitMessage(ERR_COMMAND);
  }

} // Comms_MessageHandler

// **************************************************************************
//
//  FUNCTION  : USART1_IRQHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles Interrupts on the iMet-1-RSB USART
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
void USART1_IRQHandler(void)
{
  // Receive Interrupt
  if(USART_GetITStatus(COMMS_USART, USART_IT_RXNE) != RESET)
  {
    // Reception Handler
    ReceptionHandler();
  }

  // Transmit Interrupt
  if(USART_GetITStatus(COMMS_USART, USART_IT_TXE) != RESET)
  {
    // Transmission Handler
    TransmissionHandler();
  }
} // USART1_IRQHandler

#endif
