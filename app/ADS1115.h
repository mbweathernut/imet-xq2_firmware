#ifndef __ADS1115_H
#define __ADS1115_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   ADS1115.h
//
//      CONTENTS    :   Header file for ADS1115
//
// **************************************************************************
#ifdef __ADS1115_C
#define ADCLOCN
#else
#define ADCLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address << 1
//#define  I2C_ADC_ID             0x92         // 1001000 << 1 if ADDR pin is VCC

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  ADS1115_Address_GND = 0x90,
  ADS1115_Address_VDD = 0x92,
  ADS1115_Address_SDA = 0x94,
  ADS1115_Address_SCL = 0x96
} TypeDef_ADS1115_Address;

typedef enum
{
  ADS1115_MeasType_SingleEnded,
  ADS1115_MeasType_Differential
} TypeDef_ADS1115_MeasType;

typedef enum
{
  ADS1115_Input_AIN0 = 0,
  ADS1115_Input_AIN1 = 1,
  ADS1115_Input_AIN2 = 2,
  ADS1115_Input_AIN3 = 3
} TypeDef_ADS1115_Input;

typedef enum
{
  ADS1115_FS_6144,
  ADS1115_FS_4096,
  ADS1115_FS_2048,
  ADS1115_FS_1024,
  ADS1115_FS_0512,
  ADS1115_FS_0256
} TypeDef_ADS1115_FS;

typedef enum
{
  ADS1115_DataRate_8SPS,
  ADS1115_DataRate_16SPS,
  ADS1115_DataRate_32SPS,
  ADS1115_DataRate_64SPS,
  ADS1115_DataRate_128SPS,
  ADS1115_DataRate_250SPS,
  ADS1115_DataRate_475SPS,
  ADS1115_DataRate_860SPS,
} TypeDef_ADS1115_DataRate;

typedef struct
{
  TypeDef_ADS1115_MeasType Type;
  TypeDef_ADS1115_Input Input;
  TypeDef_ADS1115_FS FS;
  TypeDef_ADS1115_DataRate DataRate;
} sADS1115_Config;

typedef struct
{
  // I2C Address
  TypeDef_ADS1115_Address Address;
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Configuration
  sADS1115_Config* Config;
} sADS1115;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
ADCLOCN void ADS1115_Init(sADS1115* ADCStructure);
ADCLOCN void ADS1115_SetConfig(sADS1115* ADC_Structure);
ADCLOCN FlagStatus ADS1115_GetConvStatus(sADS1115* ADC_Structure);
ADCLOCN void ADS1115_StartConversion(sADS1115* ADC_Structure);
ADCLOCN float ADS1115_GetVoltage(sADS1115* ADC_Structure);

#endif
