#ifndef __PERIPHERALS_H
#define __PERIPHERALS_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   peripherals.h
//
//      CONTENTS    :   Header file for iMet-X Control Board Peripherals
//
// **************************************************************************

#ifdef __PERIPHERALS_C
#define PERIPHLOCN
#else
#define PERIPHLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// LED Hardware
// Blue LED (Radiosonde Data)
#define LED_BLU_PORT          GPIOF
#define LED_BLU_PIN           GPIO_Pin_7
#define LED_BLU_RCC           RCC_AHBPeriph_GPIOF

#define PB_ST_INIT            0
#define PB_ST_ON              1
#define PB_ST_SHUTDOWN        2
#define PB_ST_BYPASS          3

// uCTL ON Output Line
#define PWR_ON_PORT           GPIOE
#define PWR_ON_PIN            GPIO_Pin_8
#define PWR_ON_RCC            RCC_AHBPeriph_GPIOE

// Pushbutton Detect Input
#define PB_IN_PORT            GPIOB
#define PB_IN_PIN             GPIO_Pin_1
#define PB_IN_RCC             RCC_AHBPeriph_GPIOB

// Wait peripherals
#define WAIT_TIM              TIM13
#define WAIT_RCC              RCC_APB1Periph_TIM13
#define WAIT_TIM_IRQ          TIM13_IRQn
#define WAIT_FREQ             100000  // 10 kHZ

// Total number of wait channels - if you add a channel, increase this!
#define WAIT_CH_CNT           5
// Wait channel definitions
#define WAIT_PRESSURE_CH      0
#define WAIT_NTC_CH           1
#define WAIT_HYT_CH           2
#define WAIT_UBX_CH           3
#define WAIT_LED_CH           4

// Data Report Rate Timer
#define DRR_TIM               TIM2
#define DRR_TIM_RCC           RCC_APB1Periph_TIM2
#define DRR_TIM_IRQ           TIM2_IRQn
#define DRR_TIM_FREQ          1000  // 1 kHz
#define DRR_DEFAULT_PERIOD    1000  // 1 sec

// User Timer
#define USER_TIM              TIM4
#define USER_TIM_RCC          RCC_APB1Periph_TIM4
#define USER_TIM_IRQ          TIM4_IRQn
#define USER_TIM_FREQ         1000  // 1 kHz

// Sensor I2C Peripherals
#define SENS_I2C              I2C1
#define SENS_RCC_SYSCLK       RCC_I2C1CLK_SYSCLK
#define SENS_RCC              RCC_APB1Periph_I2C1

#define SENS_SCL_PORT_RCC     RCC_AHBPeriph_GPIOB
#define SENS_SCL_PORT         GPIOB
#define SENS_SCL_PIN          GPIO_Pin_8
#define SENS_SCL_PINSRC       GPIO_PinSource8
#define SENS_SCL_AF           GPIO_AF_4

#define SENS_SDA_PORT_RCC     RCC_AHBPeriph_GPIOB
#define SENS_SDA_PORT         GPIOB
#define SENS_SDA_PIN          GPIO_Pin_9
#define SENS_SDA_PINSRC       GPIO_PinSource9
#define SENS_SDA_AF           GPIO_AF_4

#define UCTL_ON_DELAY         2  // seconds

// Watchdog peripherals
#define WWDG_MAX_CNT          0x7F

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  LED_OFF = 0,
  LED_ON = 1,
  LED_BLINK = 2,
  LED_FAST_BLINK = 3
} Type_LED_State;

typedef struct
{
  uint8_t State;
  FlagStatus Flag_ButtonPressed;
  FlagStatus Flag_Startup;
  FlagStatus Flag_Shutdown;
  uint32_t TicsCounter;
} sPushbutton;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
PERIPHLOCN FlagStatus Flag_DRR_Disable;
PERIPHLOCN FlagStatus Flag_DisableWWDG;
PERIPHLOCN sPushbutton Pushbutton;
PERIPHLOCN Type_LED_State LED_State;
extern uint16_t Wait_Tics;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
// Initializes all the timers
PERIPHLOCN void InitPeripherals(void);
PERIPHLOCN void Periph_I2C_FaultHandler(I2C_TypeDef* I2Cx);
// Initialize the watchdog timer
PERIPHLOCN void Periph_InitWatchdog(void);
// Pushbutton Functions
PERIPHLOCN void Pushbutton_Init(sPushbutton* Button);
PERIPHLOCN void Pushbutton_Handler(sPushbutton* Button);
// LED functions
PERIPHLOCN void SetLEDState(Type_LED_State State);
PERIPHLOCN void LED_Handler(void);
// Precise sleep functions
PERIPHLOCN void Wait(uint8_t Channel, uint16_t ms);
PERIPHLOCN FlagStatus GetWaitFlagStatus(uint8_t Channel);
PERIPHLOCN void WaitHandler(void);
// Data report rate functions
PERIPHLOCN void StartDRRTimer(void);
PERIPHLOCN void StopDRRTimer(void);
PERIPHLOCN FlagStatus GetDRRFlagStatus(void);
PERIPHLOCN void ClearDRRFlag(void);

#endif /* __PERIPHERALS_H */
