#ifndef __THERMISTOR_C
#define __THERMISTOR_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-XQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   thermistor.c
//
//      CONTENTS    :   Routines for thermistor state machine and calculations
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  INVALID_TEMP         -99999
#define  WING_RESISTOR        64900.0

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static sADS1115_Config VCC_Config;
static sADS1115_Config NTC_Config;
static uint8_t ErrorCounter;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void CalculateResistance(sThermistor* ThermistorStructure);
static void CalculateTemperature(sThermistor* ThermistorStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CalculateResistance
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-05-20 JHM
//
// **************************************************************************
static void CalculateResistance(sThermistor* ThermistorStructure)
{
  ThermistorStructure->Resistance = WING_RESISTOR * (ThermistorStructure->VCC_Voltage / ThermistorStructure->NTC_Voltage - 1.0);
} // CalculateResistance

// **************************************************************************
//
//  FUNCTION  : CalculateTemperature
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-05-20 JHM
//
// **************************************************************************
static void CalculateTemperature(sThermistor* ThermistorStructure)
{
  float Temperature;
  float LogR;

  LogR = logf(ThermistorStructure->Resistance);

  // Calculate the temperature in Kelvin
  Temperature = 1 / (ThermistorStructure->Coefficients.A0 + ThermistorStructure->Coefficients.A1 * LogR + ThermistorStructure->Coefficients.A2 * powf(LogR, 2) + ThermistorStructure->Coefficients.A3 * powf(LogR, 3));

  // Convert to Celsius
  Temperature -= 273.15;

  // Set the temperature value in the structure
  ThermistorStructure->Temperature = (int32_t)(Temperature * 100);

  // Add bias
  ThermistorStructure->Temperature += ThermistorStructure->Coefficients.BiasT;
} // CalculateTemperature

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Thermistor_Init
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
void Thermistor_Init(sThermistor* ThermistorStructure)
{
  // Initialize Structure Variables
  ThermistorStructure->State = THERM_ST_OFFLINE;
  ThermistorStructure->Resistance = 0.0;
  ThermistorStructure->Temperature = INVALID_TEMP;
  ThermistorStructure->Message[0] = 0;
  ThermistorStructure->WaitChannel = WAIT_NTC_CH;

  // Load Steinhart-Hart Coefficients
  ThermistorStructure->Coefficients.A0 = iMetXQ_Config.Coefficients.A0;
  ThermistorStructure->Coefficients.A1 = iMetXQ_Config.Coefficients.A1;
  ThermistorStructure->Coefficients.A2 = iMetXQ_Config.Coefficients.A2;
  ThermistorStructure->Coefficients.A3 = iMetXQ_Config.Coefficients.A3;

  // Load Bias
  ThermistorStructure->Coefficients.BiasT = iMetXQ_Config.Coefficients.BiasT;

  // Set configurations
  // For measuring the NTC voltage on pin A0
  NTC_Config.Type = ADS1115_MeasType_SingleEnded;
  NTC_Config.Input = ADS1115_Input_AIN0;
  NTC_Config.FS = ADS1115_FS_4096;
  NTC_Config.DataRate = ADS1115_DataRate_250SPS;

  // For measuring the supply voltage on pin A1
  VCC_Config.Type = ADS1115_MeasType_SingleEnded;
  VCC_Config.Input = ADS1115_Input_AIN1;
  VCC_Config.FS = ADS1115_FS_4096;
  VCC_Config.DataRate = ADS1115_DataRate_250SPS;

  // Initialize local variables
  ErrorCounter = 0;

  // Initialize the ADC config
  ThermistorStructure->ADC.Config = &NTC_Config;

  // Initialize the rest of the ADC
  ADS1115_Init(&ThermistorStructure->ADC);

  // Set the configuration
  ADS1115_SetConfig(&ThermistorStructure->ADC);

  // Set the state to idle if successful or offline if unsuccessful
  if (ThermistorStructure->ADC.Flag_CommsFault == SET)
  {
    ThermistorStructure->State = THERM_ST_OFFLINE;
    return;
  }

  // Start the machine and get the first temperature reading
  ThermistorStructure->State = THERM_ST_START;
  do
  {
    Thermistor_Handler(ThermistorStructure);
  }
  while (ThermistorStructure->State != THERM_ST_START);
} // Thermistor_Init

// **************************************************************************
//
//  FUNCTION  : Thermistor_Handler
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
void Thermistor_Handler(sThermistor* ThermistorStructure)
{
  char strVariable[10];

  // Exit immediately if sensor is waiting
  if (GetWaitFlagStatus(ThermistorStructure->WaitChannel) == SET)
  {
    return;
  }

  // Handle Comms error if it has occurred
  if (ThermistorStructure->ADC.Flag_CommsFault == SET)
  {
    // Clear the comms fault flag
    ThermistorStructure->ADC.Flag_CommsFault = RESET;
    // Reboot the I2C interface
    Periph_I2C_FaultHandler(ThermistorStructure->ADC.I2Cx);
    // Reset the state machine
    ThermistorStructure->State = THERM_ST_START;
    Wait(ThermistorStructure->WaitChannel, 2);
    return;
  }

  switch (ThermistorStructure->State)
  {
    case THERM_ST_OFFLINE:
      break;

    case THERM_ST_START:
      // Advance the state machine
      ThermistorStructure->State = THERM_ST_VCC;
      break;

    case THERM_ST_VCC:
      // Configure for VCC measurement
       ThermistorStructure->ADC.Config = &VCC_Config;
       // Start the conversion
       ADS1115_StartConversion(&ThermistorStructure->ADC);
       // Advance the state machine
       ThermistorStructure->State = THERM_ST_GETVCC;
       // Wait 5 ms for conversion to complete
       Wait(ThermistorStructure->WaitChannel, 25);
       break;

    case THERM_ST_GETVCC:
      ThermistorStructure->ADC.Config = &VCC_Config;

      // Conversion is complete
      ThermistorStructure->VCC_Voltage = ADS1115_GetVoltage(&ThermistorStructure->ADC);
      // Increment state machine
      ThermistorStructure->State = THERM_ST_NTC;
      Wait(ThermistorStructure->WaitChannel, 2);
      break;

    case THERM_ST_NTC:
      // Configure for NTC measurement
       ThermistorStructure->ADC.Config = &NTC_Config;
       // Start the conversion
       ADS1115_StartConversion(&ThermistorStructure->ADC);
       // Increment state machine
       ThermistorStructure->State = THERM_ST_GETNTC;
       Wait(ThermistorStructure->WaitChannel, 25);
      break;

    case THERM_ST_GETNTC:
      ThermistorStructure->ADC.Config = &NTC_Config;
      // Conversion is complete
      ThermistorStructure->NTC_Voltage = ADS1115_GetVoltage(&ThermistorStructure->ADC);
      // Increment the state machine
      ThermistorStructure->State = THERM_ST_LOAD;
      Wait(ThermistorStructure->WaitChannel, 2);
      break;

    case THERM_ST_LOAD:
      // Calculate resistance from voltage
      CalculateResistance(ThermistorStructure);
      // Calculate temperature from resistance
      CalculateTemperature(ThermistorStructure);
      // Print temperature to message
      ThermistorStructure->Message[0] = 0;
      sprintSignedNumber(strVariable, ThermistorStructure->Temperature, 4);
      strcat(ThermistorStructure->Message, strVariable);

      // Update flag status
      ThermistorStructure->Flag_DataReady = SET;

      // Advance the state machine
      ThermistorStructure->State = THERM_ST_START;

      Wait(ThermistorStructure->WaitChannel, 100);
      break;

    case THERM_ST_IDLE:
      break;

    default:
      break;
  } // switch
} // Thermistor_Handler

#endif


