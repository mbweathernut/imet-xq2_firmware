// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   UBlox.c
//
//      CONTENTS    :   Routines for communication with the UBlox CAM-M8Q 
//                      Module
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************

#include "includes.h"

// Headers
#define  UBX_HEADER1            0xB5
#define  UBX_HEADER2            0x62

// Message Classes
#define  UBX_CL_NAV             0x01
#define  UBX_CL_RXM             0x02
#define  UBX_CL_INF             0x04
#define  UBX_CL_ACK             0x05
#define  UBX_CL_CFG             0x06
#define  UBX_CL_UPD             0x09
#define  UBX_CL_MON             0x0A
#define  UBX_CL_AID             0x0B
#define  UBX_CL_TIM             0x0D
#define  UBX_CL_MGA             0x13
#define  UBX_CL_LOG             0x21
#define  UBX_CL_NMEA            0xF0

// Navigation Messages
#define  UBX_NAV_PVT            0x07

// Configuration Messages
#define  UBX_CFG_RST            0x04
#define  UBX_CFG_MSG            0x01
#define  UBX_CFG_PRT            0x00

// Configuration Message IDs
#define  ID_PRT                 0x00
#define  ID_MSG                 0x01
#define  ID_RST                 0x04
#define  ID_RATE                0x08
#define  ID_NAV5                0x24
#define  ID_NAVX5               0x23

// Acknowledge Messages IDs
#define  ID_NAK                 0x00
#define  ID_ACK                 0x01

// Navigation Message IDs
#define  ID_POSECEF             0x01
#define  ID_PVT                 0x07
#define  ID_VELECEF             0x11
#define  ID_SVINFO              0x30
#define  ID_SAT                 0x35
#define  ID_SOL                 0x06
#define  ID_POSLLH              0x02

// Acknowledge Messages
#define  UBX_ACK_NAK            0x00
#define  UBX_ACK_ACK            0x01
#define  UBX_ERR_TIMEOUT        0x02
#define  UBX_ERR_CS             0x03

// Reserved bytes
#define  UBX_RESERVED           0x00

#define  NAV_PVT_BYTES          100

typedef  uint8_t AckResult;

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sUBlox_Engine UBlox;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void UBlox_WriteI2C(sUBlox_Engine* GPSStructure, uint8_t PayloadLength, uint8_t* ucpData);
static uint8_t UBlox_ReadI2C(sUBlox_Engine* GPSStructure);
static void UBlox_AddToBuffer(sUBlox_Engine* GPSStructure);
static void UBlox_ClearBuffer(sUBlox_Engine* GPSStructure);
static void UBlox_DecodePVT(sUBlox_Engine* GPSStructure);
//static void UBlox_DecodeSAT(sUBlox_Engine* GPSStructure);
static AckResult UBlox_GetAck(sUBlox_Engine* GPSStructure);
static void ConfigurePort(sUBlox_Engine* GPSStructure);
static void ConfigureMessage(sUBlox_Engine* GPSStructure, uint8_t Class, uint8_t ID);
static void ConfigureNavEngine(sUBlox_Engine* GPSStructure);
static void ConfigureNavEngineExpert(sUBlox_Engine* GPSStructure);
static FlagStatus UBlox_ValidityCheck(sUBlox_Engine* GPSStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : UBlox_WriteI2C
//
//  I/P       : UBlox Data Structure
//              PayloadLength - Number of packets
//              ucpData - Byte Array for Transmission
//
//  O/P       : None.
//
//  OPERATION : Sends a message to the UBlox engine using UBX Protocol
//
//  UPDATED   : 2015-03-03 JHM
//
// **************************************************************************
static void UBlox_WriteI2C(sUBlox_Engine* GPSStructure, uint8_t PayloadLength, uint8_t* ucpData)
{
  int i;

  // Clear the NACK flag so we can check whether the device responds correctly
  I2C_ClearFlag(GPSStructure->I2Cx, I2C_FLAG_NACKF);

  // This function creates the start condition and clocks out the control byte
  I2C_TransferHandling(GPSStructure->I2Cx, I2C_UBLOX_ID, PayloadLength, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  // Send the data
  for (i = 0; i < PayloadLength; i++)
  {
    // Wait for the data to send
    while ((I2C_GetFlagStatus(GPSStructure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
           (I2C_GetFlagStatus(GPSStructure->I2Cx, I2C_FLAG_NACKF) == RESET));

    if (I2C_GetFlagStatus(GPSStructure->I2Cx, I2C_FLAG_NACKF) == SET)
    {
      // Clear the flags
      I2C_ClearFlag(GPSStructure->I2Cx, I2C_FLAG_NACKF);
      // Send the error to the structure
      GPSStructure->Flag_CommsFault = SET;
      break;
    }
    else
    {
      // Everything is ok, send the data
      I2C_SendData(GPSStructure->I2Cx, *ucpData);
    }
    ucpData++;
  }

  while (I2C_GetFlagStatus(GPSStructure->I2Cx, I2C_FLAG_STOPF) == RESET);
} // UBlox_WriteI2C

// **************************************************************************
//
//  FUNCTION  : UBlox_ReadI2C
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION : Reads and returns a single byte of data. The UBlox module
//              operates in I2C by sending a read command (along with the
//              address). If data is not available, the module will send back
//              0xFF.
//
//  UPDATED   : 2015-03-04 JHM
//
// **************************************************************************
static uint8_t UBlox_ReadI2C(sUBlox_Engine* GPSStructure)
{
  uint8_t Byte = 0xFF;
  uint16_t Timeout = 2000;

  // Set up the transmission and immediately begin sending
  I2C_TransferHandling(GPSStructure->I2Cx, I2C_UBLOX_ID, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  // Wait for transfer to complete
  while ((I2C_GetFlagStatus(GPSStructure->I2Cx, I2C_FLAG_RXNE) == RESET) && Timeout){Timeout--;}
  Byte = I2C_ReceiveData(GPSStructure->I2Cx);

  if (!Timeout)
  {
    GPSStructure->Flag_CommsFault = SET;
  }

  // Wait for transmission to complete
  while (I2C_GetFlagStatus(GPSStructure->I2Cx, I2C_FLAG_TC) == SET);

  return Byte;
} // UBlox_ReadI2C

// **************************************************************************
//
//  FUNCTION  : UBlox_ReadMessage
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION : Reads the rest of the message into the message buffer. This
//              routine should be called once the HEADER1 character is
//              detected.
//
//  UPDATED   : 2017-10-26 JHM
//
// **************************************************************************
static void UBlox_ReadMessage(sUBlox_Engine* GPSStructure)
{
  uint16_t Payload_Size = 0;
  int i;

  // Read Header 2
  UBlox_AddToBuffer(GPSStructure);

  if (GPSStructure->UBX_Message.RxBuffer[GPSStructure->UBX_Message.Index] == UBX_HEADER2)
  {
    GPSStructure->UBX_Message.Index++;
  }
  else
  {
    GPSStructure->State = UBLOX_ST_START;
    return;
  }

  // Read past class and id
  UBlox_AddToBuffer(GPSStructure);
  GPSStructure->UBX_Message.Index++;
  UBlox_AddToBuffer(GPSStructure);
  GPSStructure->UBX_Message.Index++;

  // Read the payload size
  UBlox_AddToBuffer(GPSStructure);
  Payload_Size |= GPSStructure->UBX_Message.RxBuffer[GPSStructure->UBX_Message.Index];
  GPSStructure->UBX_Message.Index++;
  UBlox_AddToBuffer(GPSStructure);
  Payload_Size |= (uint16_t)GPSStructure->UBX_Message.RxBuffer[GPSStructure->UBX_Message.Index] << 8;
  GPSStructure->UBX_Message.Index++;

  // Read the rest of the message
  for (i = 0; i < Payload_Size; i++)
  {
    UBlox_AddToBuffer(GPSStructure);
    GPSStructure->UBX_Message.Index++;
  } // for

  // Read the checksums
  UBlox_AddToBuffer(GPSStructure);
  GPSStructure->UBX_Message.Index++;
  UBlox_AddToBuffer(GPSStructure);
} // UBlox_ReadMessage

// **************************************************************************
//
//  FUNCTION  : UBlox_AddToBuffer
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2015-03-05 JHM
//
// **************************************************************************
static void UBlox_AddToBuffer(sUBlox_Engine* GPSStructure)
{
  // Add the data to the buffer
  GPSStructure->UBX_Message.RxBuffer[GPSStructure->UBX_Message.Index] = UBlox_ReadI2C(GPSStructure);

  if (GPSStructure->Flag_CommsFault == SET)
  {
    GPSStructure->Flag_CommsFault = SET;
    return;
  }
} // UBlox_AddToBuffer

// **************************************************************************
//
//  FUNCTION  : UBlox_ClearBuffer
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2015-03-05 JHM
//
// **************************************************************************
static void UBlox_ClearBuffer(sUBlox_Engine* GPSStructure)
{
  int i;

  // Reset the counters
  GPSStructure->UBX_Message.Index = 0;

  // Clear the buffer
  for (i = 0; i < UBLOX_RX_SIZE; i++)
  {
    GPSStructure->UBX_Message.RxBuffer[i] = 0;
  }
} // UBlox_ClearBuffer

// **************************************************************************
//
//  FUNCTION  : UBlox_DecodeMessage
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2015-03-05 JHM
//
// **************************************************************************
static void UBlox_DecodePVT(sUBlox_Engine* GPSStructure)
{
  char DataField[10];
  DataField[0] = 0;
  uint8_t* ptr;

  // Move the pointer past the header info
  ptr = &GPSStructure->UBX_Message.RxBuffer[6];

  // Time of week
  memcpy(&GPSStructure->PVT_Data.iTOW, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);
  // Year
  memcpy(&GPSStructure->PVT_Data.year, ptr, sizeof(uint16_t));
  ptr += sizeof(uint16_t);
  // Month
  memcpy(&GPSStructure->PVT_Data.month,ptr, 1);
  ptr++;
  // Day
  memcpy(&GPSStructure->PVT_Data.day, ptr, 1);
  ptr++;
  // Hour
  memcpy(&GPSStructure->PVT_Data.hour, ptr, 1);
  ptr++;
  // Minute
  memcpy(&GPSStructure->PVT_Data.min, ptr, 1);
  ptr++;
  // Second
  memcpy(&GPSStructure->PVT_Data.sec, ptr, 1);
  ptr++;
  // Valid
  memcpy(&GPSStructure->PVT_Data.valid, ptr, 1);
  ptr++;
  // Time accuracy estimate
  memcpy(&GPSStructure->PVT_Data.tAcc, ptr, 4);
  ptr += 4;
  // Fraction of second
  memcpy(&GPSStructure->PVT_Data.nano, ptr, 4);
  ptr += 4;
  // GNSSfix Type
  memcpy(&GPSStructure->PVT_Data.fixType, ptr, 1);
  ptr++;
  // Flags
  memcpy(&GPSStructure->PVT_Data.flags, ptr, 1);
  ptr++;
  // Reserved
  ptr++;
  // SV#
  memcpy(&GPSStructure->PVT_Data.numSV, ptr, 1);
  ptr++;
  // Longitude
  memcpy(&GPSStructure->PVT_Data.lon, ptr, 4);
  ptr += 4;
  // Latitude
  memcpy(&GPSStructure->PVT_Data.lat, ptr, 4);
  ptr += 4;
  // Altitude (EES)
  memcpy(&GPSStructure->PVT_Data.height, ptr, 4);
  ptr += 4;
  // Altitude (MSL)
  memcpy(&GPSStructure->PVT_Data.hMSL, ptr, 4);
  //ptr += 4;

  // Move the data from the buffer to the structure and build the message
  GPSStructure->Message[0] = 0;
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.year,4);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, "/");
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.month,2);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, "/");
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.day,2);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ",");
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.hour,2);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ":");
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.min,2);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ":");
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.sec,2);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ",");
  sprintSignedNumber(DataField, GPSStructure->PVT_Data.lon, 10);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ",");
  sprintSignedNumber(DataField, GPSStructure->PVT_Data.lat, 10);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ",");
  sprintSignedNumber(DataField, GPSStructure->PVT_Data.hMSL , 8);
  strcat(GPSStructure->Message, DataField);
  strcat(GPSStructure->Message, ",");
  sprintUnsignedNumber(DataField, GPSStructure->PVT_Data.numSV, 2);
  strcat(GPSStructure->Message, DataField);
} // UBlox_DecodeMessage
/*
// **************************************************************************
//
//  FUNCTION  : UBlox_DecodeSAT
//
//  I/P       : sUBlox_Engine* GPSStructure
//
//  O/P       : None.
//
//  OPERATION : Decodes the satellite information
//
//  UPDATED   : 2016-11-28 JHM
//
// **************************************************************************
static void UBlox_DecodeSAT(sUBlox_Engine* GPSStructure)
{
  char DataField[10];
  DataField[0] = 0;
  uint8_t* ptr;
  int i;

  // Move the pointer past the header info
  ptr = &GPSStructure->UBX_Message.RxBuffer[6];

  // GPS time of week
  memcpy(&GPSStructure->SAT_Data.iTOW, ptr, sizeof(GPSStructure->SAT_Data.iTOW));
  ptr += sizeof(GPSStructure->SAT_Data.iTOW);
  // Skip version
  ptr++;
  // Number of satellites
  memcpy(&GPSStructure->SAT_Data.numSvs, ptr, sizeof(GPSStructure->SAT_Data.numSvs));
  ptr++;
  // Skip reserved bytes
  ptr++;
  ptr++;

  for (i = 0; i < GPSStructure->SAT_Data.numSvs; i++)
  {
    GPSStructure->SAT_Data.Satellites[i].gnssId = *ptr;
    ptr++;
    GPSStructure->SAT_Data.Satellites[i].svId = *ptr;
    ptr++;
    GPSStructure->SAT_Data.Satellites[i].cno = *ptr;
    ptr++;
    GPSStructure->SAT_Data.Satellites[i].elev = *ptr;
    ptr++;
    memcpy(&GPSStructure->SAT_Data.Satellites[i].azim, ptr, 2);
    ptr += 2;
    memcpy(&GPSStructure->SAT_Data.Satellites[i].prRes, ptr, 2);
    ptr += 2;
    // Skip flags
    ptr += 4;
  }

  // Move the data from the buffer to the structure and build the message
  GPSStructure->Message[0] = 0;
  //sprintUnsignedNumber(DataField, GPSStructure->SAT_Data.iTOW / 1000, 5);
  //strcat(GPSStructure->Message, DataField);
  //strcat(GPSStructure->Message, ",");
  sprintUnsignedNumber(DataField, GPSStructure->SAT_Data.numSvs, 2);
  strcat(GPSStructure->Message, DataField);

  for (i = 0; i < UBX_MAX_SAT; i++)
  {
    if (GPSStructure->SAT_Data.Satellites[i].gnssId == 0)
    {
      strcat(GPSStructure->Message, ",[");
      sprintUnsignedNumber(DataField, GPSStructure->SAT_Data.Satellites[i].svId, 2);
      strcat(GPSStructure->Message, DataField);
      strcat(GPSStructure->Message, "]");
      sprintUnsignedNumber(DataField, GPSStructure->SAT_Data.Satellites[i].cno, 2);
      strcat(GPSStructure->Message, DataField);
    }

    if (i == GPSStructure->SAT_Data.numSvs)
    {
      break;
    }
  }

} // UBlox_DecodeSAT
*/
// **************************************************************************
//
//  FUNCTION  : UBlox_GetAck
//
//  I/P       : UBlox Data Structure
//
//  O/P       : AckResult
//              UBX_ACK_NAK(0x00) = Command not recognized by the UBlox Rx
//              UBX_ACK_ACK(0x01) = Message received successfully
//              UBX_ERR_TIMEOUT(0x02) = Module timeout (received 0xFF more than X times)
//              UBX_ERR_CS(0x03) = Checksum Error
//
//  OPERATION : This function is meant to be called directly following a
//              configuration command. After a command is sent to the module,
//              the module should respond with an ACK message. If anything
//              other than UBX_ACK_ACK is returned, an error has occurred.
//
//  UPDATED   : 2015-03-04 JHM
//
// **************************************************************************
static AckResult UBlox_GetAck(sUBlox_Engine* GPSStructure)
{
  uint8_t Result[10];
  uint8_t Byte;
  uint16_t Checksum;
  int Timeout;
  int i;

  Timeout = 1000;
  while (Timeout)
  {
    // Poll the GPS engine - it will report 0xFF if the data is not available
    Byte = UBlox_ReadI2C(GPSStructure);

    if (Byte == 0xFF)
    {
      // No data available, decrement timeout
      Timeout--;
    }
    else
    {
      //Start of a new message, break and start to read
      Result[0] = Byte;
      break;
    }
  }
  // If a timeout has occurred, return with error
  if (Timeout == 0)
  {
    return UBX_ERR_TIMEOUT;
  }

  // Read up to UBX message if necessary
  Header1:
  if (Result[0] == 0xB5)
  {
    Result[1] = UBlox_ReadI2C(GPSStructure);
    goto Header2;
  }
  else
  {
    Result[0] = UBlox_ReadI2C(GPSStructure);
    goto Header1;
  }

  Header2:
  if (Result[1] == 0x62)
  {
    goto End;
  }
  else
  {
    Result[0] = Result[1];
    goto Header1;
  }

  End:

  // Add in the rest of the ACK response
  for (i = 2; i < sizeof(Result); i++)
  {
    Result[i] = UBlox_ReadI2C(GPSStructure);
  }

  // Forget about checksums for now
  Checksum = Fletcher8Bit(Result, 2, 7);

  if ((Checksum & 0xFF) != Result[8])
  {
    return UBX_ERR_CS;
  }

  if ((Checksum >> 8) != Result[9])
  {
    return UBX_ERR_CS;
  }

  // Return the element of the message representing ACK or NAK
  // ACK = 0x01
  // NAK = 0x00
  return Result[3];
} // UBlox_GetAck

// **************************************************************************
//
//  FUNCTION  : ConfigurePort
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION : Configures the I2C output of the module to NMEA disabled and
//              UBX active. The GetAck() method is called at the end to
//              verify that the message has been received properly.
//
//  UPDATED   : 2015-03-04 JHM
//
// **************************************************************************
static void ConfigurePort(sUBlox_Engine* GPSStructure)
{
  uint8_t Message[28];
  uint16_t Checksum;
  AckResult Result;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;
  // Message ID (start checksum here)
  Message[2] = UBX_CL_CFG;
  Message[3] = UBX_CFG_PRT;
  // Payload Length (Little Endian) = 20 bytes
  Message[4] = 0x14;
  Message[5] = 0x00;
  // Payload
  Message[6] = 0x00; // DDC Port ID
  Message[7] = UBX_RESERVED;
  Message[8] = 0x00; // No txReady
  Message[9] = 0x00; // No txReady
  Message[10] = 0x84; // Mode LSB
  Message[11] = 0x00; // Mode
  Message[12] = 0x00; // Mode
  Message[13] = 0x00; // Mode MSB
  Message[14] = 0x00; // Baud Rate LSB
  Message[15] = 0x00; // Baud Rate
  Message[16] = 0x00; // Baud Rate
  Message[17] = 0x00; // Baud Rate MSB
  Message[18] = 0x01; // inProtoMask LSB = u-blox only, no NMEA
  Message[19] = 0x00; // inProtoMask MSB
  Message[20] = 0x01; // outProtoMask LSB = u-blox only, no NMEA
  Message[21] = 0x00; // outProtoMask MSB
  Message[22] = 0x02; // Flags LSB(disable timeout)
  Message[23] = 0x00; // Flags MSB
  Message[24] = UBX_RESERVED;
  Message[25] = UBX_RESERVED;
  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 25);
  Message[26] = (uint8_t)(Checksum & 0xFF);
  Message[27] = (uint8_t)(Checksum >> 8);

  // Send Configure Command
  UBlox_WriteI2C(GPSStructure, sizeof(Message), Message);

  if (GPSStructure->Flag_CommsFault == RESET)
  {
    Result = UBlox_GetAck(GPSStructure);
    if (Result != UBX_ACK_ACK)
    {
      GPSStructure->Flag_CommsFault = SET;
    }
  }

  // Wait for any additional messages that may have come in
  Wait(GPSStructure->WaitChannel, 100);
  while (GetWaitFlagStatus(GPSStructure->WaitChannel) == SET);

  // Clear the RX buffer so we can start afresh
  // Read data until the output buffer of the GPS engine is empty
  while (UBlox_ReadI2C(GPSStructure) != 0xFF);

  // Clear the RX buffer
  UBlox_ClearBuffer(GPSStructure);
} // ConfigurePort

// **************************************************************************
//
//  FUNCTION  : ConfigureMessage
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION : Configures the PVT message for 1 second intervals so it will
//              automatically output the data.
//
//  UPDATED   : 2015-10-15 JHM
//
// **************************************************************************
static void ConfigureMessage(sUBlox_Engine* GPSStructure, uint8_t Class, uint8_t ID)
{
  uint8_t Message[16];
  uint16_t Checksum;
  AckResult Result;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;
  // Message ID (start checksum here)
  Message[2] = UBX_CL_CFG;
  Message[3] = UBX_CFG_MSG;
  // Payload Length (Little Endian) = 8 bytes
  Message[4] = 0x08;
  Message[5] = 0x00;
  // Payload
  Message[6] = Class;    // Navigation Class
  Message[7] = ID;
  Message[8] = 0x01;          // DDC Port - 1 second
  Message[9] = 0x00;          // Everything else off
  Message[10] = 0x00;
  Message[11] = 0x00;
  Message[12] = 0x00;
  Message[13] = 0x00;

  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 13);
  Message[14] = (uint8_t)(Checksum & 0xFF);
  Message[15] = (uint8_t)(Checksum >> 8);

  // Send Configure Command
  UBlox_WriteI2C(GPSStructure, sizeof(Message), Message);

  if (GPSStructure->Flag_CommsFault == RESET)
  {
    Result = UBlox_GetAck(GPSStructure);
    if (Result != UBX_ACK_ACK)
    {
      GPSStructure->Flag_CommsFault = SET;
    }
  }
} // ConfigureMessage

// **************************************************************************
//
//  FUNCTION  : ConfigureNavEngine
//
//  I/P       : sUBlox_Engine* GPSStructure
//
//  O/P       : None.
//
//  OPERATION : Configures the navigation engine for optimal settings for
//              radiosonde flight
//
//  UPDATED   : 2016-04-05 JHM
//
// **************************************************************************
static void ConfigureNavEngine(sUBlox_Engine* GPSStructure)
{
  uint8_t Message[44];
  AckResult Result;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = UBX_CL_CFG;
  Message[3] = ID_NAV5;

  // Payload Length (Little Endian) = 36 bytes
  Message[4] = 0x24;
  Message[5] = 0x00;

  // Payload
  // Mask (all set to apply all parameters)
  Message[6] = 0xFF;
  Message[7] = 0xFF;
  // Dynamic model = airborne with <2g acceleration
  Message[8] = 0x07;
  // Fix mode = 3D only
  Message[9] = 0x2;
  // Fixed altitude for 2D fix mode = 0 m
  Message[10] = 0;
  Message[11] = 0;
  Message[12] = 0;
  Message[13] = 0;
  // Fixed altitude variance for 2D mode = 10,000 m^2
  Message[14] = 0x10;
  Message[15] = 0x27;
  Message[16] = 0x00;
  Message[17] = 0x00;
  // Minimum elevation = 0 degrees
  Message[18] = 0x00;
  // Reserved
  Message[19] = UBX_RESERVED;
  // Position DOP Mask to use
  Message[20] = 0xFA;
  Message[21] = 0x00;
  // Time DOP Mask to use
  Message[22] = 0xFA;
  Message[23] = 0x00;
  // Position Accuracy Mask
  Message[24] = 0x64;
  Message[25] = 0x00;
  // Time Accuracy Mask
  Message[26] = 0x2C;
  Message[27] = 0x01;
  // Static hold threshold = 0 cm/s
  Message[28] = 0x00;
  // DGPS timeout = 60 seconds
  Message[29] = 0x3C;
  // Number of satellites required to have C/N0 above cnoThresh for
  // a fix to be attempted = 0
  Message[30] = 0;
  // C/N0 threshold for deciding whether to attempt a fix = 0
  Message[31] = 0;
  // Reserved
  Message[32] = UBX_RESERVED;
  Message[33] = UBX_RESERVED;
  // Static hold distance threshold = 200 m
  Message[34] = 0xC8;
  Message[35] = 0x00;
  // UTC standard to be used = Unspecified
  Message[36] = 0;
  // Reserved
  Message[37] = UBX_RESERVED;
  Message[38] = UBX_RESERVED;
  Message[39] = UBX_RESERVED;
  Message[40] = UBX_RESERVED;
  Message[41] = UBX_RESERVED;

  // Calculate Checksum (this failed, I had to use the checksum from the
  // development board)
  Message[42] = 0x15;
  Message[43] = 0xB1;

  // Send Configure Command
  UBlox_WriteI2C(GPSStructure, sizeof(Message), Message);

  if (GPSStructure->Flag_CommsFault == RESET)
  {
    Result = UBlox_GetAck(GPSStructure);
    if (Result != UBX_ACK_ACK)
    {
      GPSStructure->Flag_CommsFault = SET;
    }
  }
} // ConfigureNavEngine

// **************************************************************************
//
//  FUNCTION  : ConfigureNavEngineExpert
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the navigation engine minimum and maximum SV info
//
//  UPDATED   : 2015-11-09 JHM
//
// **************************************************************************
static void ConfigureNavEngineExpert(sUBlox_Engine* GPSStructure)
{
  uint8_t Message[48];
  uint16_t Checksum;
  AckResult Result;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = UBX_CL_CFG;
  Message[3] = ID_NAVX5;

  // Payload Length (Little Endian) = 40 bytes
  Message[4] = 0x28;
  Message[5] = 0x00;

  // Payload
  // Version = set to zero for this version
  Message[6] = 0x00;
  Message[7] = 0x00;

  // Mask 1 = Set all possible masks to load each configuration option
  Message[8] = 0x4C;
  Message[9] = 0x66;

  // Mask 2 = all zeros
  Message[10] = 0;
  Message[11] = 0;
  Message[12] = 0;
  Message[13] = 0;

  // Reserved bytes
  Message[14] = UBX_RESERVED;
  Message[15] = UBX_RESERVED;

  // Minimum number of satellites for navigation = 4
  Message[16] = 0x04;
  // Maximum number of satellites = 16
  Message[17] = 0x10;
  // Minimum CNO = default
  Message[18] = 0x06;

  // Reserved
  Message[19] = UBX_RESERVED;

  // Initial fix must be 3D
  Message[20] = 0x01;

  // Reserved
  Message[21] = UBX_RESERVED;
  Message[22] = UBX_RESERVED;

  // Ack Aiding = do not issue acknowledgements for ack aiding
  Message[23] = 0;

  // GPS week rollover = default
  Message[24] = 0xDC;
  Message[25] = 0x06;

  // Reserved
  Message[26] = UBX_RESERVED;
  Message[27] = UBX_RESERVED;
  Message[28] = UBX_RESERVED;
  Message[29] = UBX_RESERVED;
  Message[30] = UBX_RESERVED;
  Message[31] = UBX_RESERVED;

  // Do not use PPP
  Message[32] = 0;
  // Do not use AssistNow
  Message[33] = 0;

  Message[34] = UBX_RESERVED;
  Message[35] = UBX_RESERVED;

  // Maximum acceptable orbit error = default
  Message[36] = 0x64;
  Message[37] = 0x00;

  Message[38] = UBX_RESERVED;
  Message[39] = UBX_RESERVED;
  Message[40] = UBX_RESERVED;
  Message[41] = UBX_RESERVED;
  Message[42] = UBX_RESERVED;
  Message[43] = UBX_RESERVED;
  Message[44] = UBX_RESERVED;

  // Do not use address
  Message[45] = 0;

  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 45);
  Message[46] = (uint8_t)(Checksum & 0xFF);
  Message[47] = (uint8_t)(Checksum >> 8);


  // Send Configure Command
  UBlox_WriteI2C(GPSStructure, sizeof(Message), Message);

  if (GPSStructure->Flag_CommsFault == RESET)
  {
    Result = UBlox_GetAck(GPSStructure);
    if (Result != UBX_ACK_ACK)
    {
      GPSStructure->Flag_CommsFault = SET;
    }
  }
} // ConfigureNavEngineExpert

// **************************************************************************
//
//  FUNCTION  : UBlox_ValidityCheck
//
//  I/P       : UBlox Data Structure
//
//  O/P       : FlagStatus:
//              SET = Valid
//              RESET = Invalid
//
//  OPERATION : Checks the data structure for valid data and returns a flag
//              indicating the validity.
//
//  UPDATED   : 2015-08-12 JHM
//
// **************************************************************************
static FlagStatus UBlox_ValidityCheck(sUBlox_Engine* GPSStructure)
{
  // Fail if we have less than four satellites
  if (GPSStructure->PVT_Data.numSV < 4)
  {
    return RESET;
  }
  return SET;
} // UBlox_ValidityCheck

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : UBlox_Init
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPS engine structure values and configures
//              the port and messages.
//
//  UPDATED   : 2015-03-04 JHM
//
// **************************************************************************
void UBlox_Init(sUBlox_Engine* GPSStructure)
{
  // Initialize the Structure
  GPSStructure->State = UBLOX_ST_OFFLINE;
  GPSStructure->Flag_CommsFault = RESET;
  GPSStructure->Flag_Valid = RESET;
  GPSStructure->Flag_DataReady = RESET;
  GPSStructure->Message[0] = 0;
  GPSStructure->WaitChannel = WAIT_UBX_CH;

  // Initialize message
  UBlox_ClearBuffer(GPSStructure);

  // Reset the engine
  UBlox_Reset(GPSStructure);

  // Configure the port for DDC (I2C)
  ConfigurePort(GPSStructure);
  DelayMs(100);

  if (GPSStructure->Flag_CommsFault == SET)
  {
    Comms_TransmitMessage("Ublox engine failed configure port.\r\n");
    while (1);
  }
  else
  {
    // Configure the NAV engine
    ConfigureNavEngine(GPSStructure);
    DelayMs(100);
  }

  if (GPSStructure->Flag_CommsFault == SET)
  {
    Comms_TransmitMessage("Ublox engine failed NAV5 configuration.\r\n");
    while (1);
  }
  else
  {
    // Configure the NAV engine
    ConfigureNavEngineExpert(GPSStructure);
    DelayMs(100);
  }


  if (GPSStructure->Flag_CommsFault == SET)
  {
    Comms_TransmitMessage("Ublox engine failed NAV5X configuration.\r\n");
    while (1);
  }
  else
  {
    // Configure the PVT message at 1 sec
    ConfigureMessage(GPSStructure, UBX_CL_NAV, ID_PVT);
    DelayMs(100);
  }

  if (GPSStructure->Flag_CommsFault == SET)
  {
    Comms_TransmitMessage("Ublox engine failed PVT configuration.\r\n");
    while (1);
  }
  else
  {
    // Configure the PVT message at 1 sec
    //ConfigureMessage(GPSStructure, UBX_CL_NAV, ID_SAT);
    //DelayMs(100);
  }

  if (GPSStructure->Flag_CommsFault == SET)
  {
    Comms_TransmitMessage("Ublox engine failed SAT configuration.\r\n");
    while(1);
  }
  else
  {
    // Sensor is online
    GPSStructure->State = UBLOX_ST_IDLE;
    DelayMs(10);
  }
} // UBlox_Init

// **************************************************************************
//
//  FUNCTION  : UBlox_Reset
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION : Immediately performs a hardware reset on the GPS engine.
//
//  UPDATED   : 2015-10-15 JHM
//
// **************************************************************************
void UBlox_Reset(sUBlox_Engine* GPSStructure)
{
  uint8_t Message[12];
  uint16_t Checksum;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;
  // Message ID (start checksum here)
  Message[2] = UBX_CL_CFG;
  Message[3] = UBX_CFG_RST;
  // Payload Length (Little Endian) = 4 bytes
  Message[4] = 0x04;
  Message[5] = 0x00;
  // Payload
  Message[6] = 0x00;  // Hot Start 1
  Message[7] = 0x00;  // Hot Start 2
  Message[8] = 0x00;  // Hardware Reset (Watchdog) immediately
  Message[9] = UBX_RESERVED;  // Reserved
  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 9);
  Message[10] = (uint8_t)(Checksum & 0xFF);
  Message[11] = (uint8_t)(Checksum >> 8);

  // Send Reset Command
  UBlox_WriteI2C(GPSStructure, sizeof(Message), Message);

  // Reset command may not acknowledge, so we just have to wait
  Wait(GPSStructure->WaitChannel, 500);
  while (GetWaitFlagStatus(GPSStructure->WaitChannel) == SET);
} // UBlox_Reset

// **************************************************************************
//
//  FUNCTION  : UBlox_Handler
//
//  I/P       : UBlox Data Structure
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2015-03-04 JHM
//
// **************************************************************************
void UBlox_Handler(sUBlox_Engine* GPSStructure)
{

  // Immediately return if the state machine is waiting
  if (GetWaitFlagStatus(GPSStructure->WaitChannel) == SET)
  {
    return;
  }

  if (GPSStructure->Flag_CommsFault == SET)
  {
    // Clear the flag
    GPSStructure->Flag_CommsFault = RESET;
    // Go to the fault state
    GPSStructure->State = UBLOX_ST_START;
    // Reboot the I2C interface
    Periph_I2C_FaultHandler(GPSStructure->I2Cx);
    Wait(GPSStructure->WaitChannel, 2);
    Comms_TransmitMessage("UBX COMMS Fault\r\n");
    return;
  }

  switch (GPSStructure->State)
  {
    case UBLOX_ST_IDLE:
      break;

    case UBLOX_ST_START:
      // Clear the Receive Buffer
      GPSStructure->UBX_Message.Index = 0;
      // Start looking for header1
      GPSStructure->State = UBLOX_ST_HEADER1;
      break;

    case UBLOX_ST_HEADER1:
      // Check for header1
      UBlox_AddToBuffer(GPSStructure);
      if (GPSStructure->UBX_Message.RxBuffer[GPSStructure->UBX_Message.Index] == UBX_HEADER1)
      {
        GPSStructure->UBX_Message.Index++;
        UBlox_ReadMessage(GPSStructure);
        GPSStructure->State = UBLOX_ST_LOAD;
      }
      else
      {
        GPSStructure->UBX_Message.Index = 0;
      }
      Wait(GPSStructure->WaitChannel, 2);
      break;

    case UBLOX_ST_LOAD:
      //UBlox_DecodeSAT(GPSStructure);
      if (GPSStructure->UBX_Message.RxBuffer[3] == 0x07)
      {
        UBlox_DecodePVT(GPSStructure);
      }

      // Clear the buffer
      UBlox_ClearBuffer(GPSStructure);

      // Check for data validity
      GPSStructure->Flag_Valid = UBlox_ValidityCheck(GPSStructure);

      // Set the data ready flag
      GPSStructure->Flag_DataReady = SET;

      // Increment state machine
      GPSStructure->State = UBLOX_ST_START;

      Wait(GPSStructure->WaitChannel, 100);
      break;

    default:
      break;
  }  // switch (GPSStructure->State)
} // UBlox_Handler
