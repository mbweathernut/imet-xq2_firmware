#ifndef __COMMS_H
#define __COMMS_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   comms.h
//
//      CONTENTS    :   Header file for communication through the serial port
//
//
// **************************************************************************

#ifdef __COMMS_C
#define COMMSLOCN
#else
#define COMMSLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// Serial Port Peripheral Configuration
#define COMMS_USART          USART1
#define COMMS_USART_RCC      RCC_APB2Periph_USART1
#define COMMS_USART_IRQ      USART1_IRQn
#define COMMS_USART_BAUD     57600

// Port IO Hardware Configuration
#define COMMS_PORT           GPIOA
#define COMMS_TX_PIN         GPIO_Pin_9
#define COMMS_RX_PIN         GPIO_Pin_10
#define COMMS_PORT_RCC       RCC_AHBPeriph_GPIOA
#define COMMS_TX_PIN_SRC     GPIO_PinSource9
#define COMMS_RX_PIN_SRC     GPIO_PinSource10
#define COMMS_TX_AF          GPIO_AF_7
#define COMMS_RX_AF          GPIO_AF_7

// Buffer Sizes
#define COMMS_TX_BUF_SIZE    300
#define COMMS_RX_BUF_SIZE    300

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint8_t Start;
  uint8_t End;
  uint8_t TxBuffer[COMMS_TX_BUF_SIZE];
} sComms_Tx;

typedef struct
{
  uint8_t Start;
  uint8_t End;
  uint8_t RxBuffer[COMMS_RX_BUF_SIZE];
  uint8_t MsgAvailable;
} sComms_Rx;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sComms_Tx Comms_Tx;
sComms_Rx Comms_Rx;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
COMMSLOCN void Comms_Init(void);
COMMSLOCN void Comms_TransmitMessage(char* Message);
COMMSLOCN void Comms_TransmitBinary(uint8_t* Data, uint16_t Length);
COMMSLOCN void Comms_MessageHandler(void);

#endif /* __COMMSRSB_H */
