#ifndef __PNP_MAIN_C
#define __PNP_MAIN_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// *************************************************************************
//
//        CONSTANTS
//
// *************************************************************************
#define FW_VERSION        "iMet-XQ2 v1.04\r\n"

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static char ASCII_Message[200];

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitializeSensors(void);

// **************************************************************************
//
//  FUNCTION  : main
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Main loop
//
//  UPDATED   : 2015-03-10 JHM
//
// **************************************************************************
int main(void)
{
  // Initialize local variables
  ASCII_Message[0] = 0;

  // Initialize the peripherals
  InitPeripherals();

  // Initialize serial port
  Comms_Init();

  // I'm alive
  Comms_TransmitMessage(FW_VERSION);

  // Initialize the pushbutton (peripherals.h)
  Pushbutton_Init(&Pushbutton);

  while (Pushbutton.State == PB_ST_INIT)
  {
    Pushbutton_Handler(&Pushbutton);
  }

  // Get the configuration
  Config_Init();

  // Initialize the memory
  Spansion_Init(&Spansion);

  // Initialize Sensors
  InitializeSensors();

  // Main loop
  while (1)
  {
    // Handle peripherals
    Pushbutton_Handler(&Pushbutton);
    LED_Handler();

    if (Comms_Rx.MsgAvailable > 0)
    {
      // Handle the message
      Comms_MessageHandler();

      // Reset the packet counter in case we have changed data rate
      PacketCounter = 0;
    }

    // Handle sensor state machines
    MS5607_Handler(&PressureSensor);
    Thermistor_Handler(&Thermistor);
    HYT271_Handler(&HumiditySensor);
    UBlox_Handler(&UBlox);

    // Handle memory state machine
    Spansion_Handler(&Spansion);

    if (UBlox.Flag_DataReady == SET)
    {
      // Clear the UBlox data ready flag
      UBlox.Flag_DataReady = RESET;

      // Increment the local packet counter variable
      PacketCounter++;

      // Set the new LED state
      if (Spansion.Flag_MemoryFull == SET)
      {
        // Alert user the memory is full with fast LED
        SetLEDState(LED_FAST_BLINK);
      }
      else if (UBlox.Flag_Valid == SET)
      {
        // Solid LED for GPS acquired and data is being saved
        if (LED_State != LED_ON)
        {
          SetLEDState(LED_ON);
        }
      }
      else
      {
        if (LED_State != LED_BLINK)
        {
          // Alert user GPS is unavailable
          SetLEDState(LED_BLINK);
        }
      }

      // Save the data packet if we are at the report rate
      if ((PacketCounter == iMetXQ_Config.DataReportRate) && (Flag_DRR_Disable == RESET))
      {
        // Reset the packet counter
        PacketCounter = 0;

        // Build the ASCII message
        ASCII_Message[0] = 0;
        strcat(ASCII_Message, "XQ,");

        strcat(ASCII_Message, PressureSensor.Message);
        strcat(ASCII_Message, ",");
        strcat(ASCII_Message, Thermistor.Message);
        strcat(ASCII_Message, ",");
        strcat(ASCII_Message, HumiditySensor.Message);
        strcat(ASCII_Message, ",");
        strcat(ASCII_Message, UBlox.Message);
        strcat(ASCII_Message, "\r\n");

        // Transmit the ASCII message via the serial port
        Comms_TransmitMessage(ASCII_Message);

        // Build the packet for storage in memory
        Packet.Year = UBlox.PVT_Data.year;
        Packet.Month = UBlox.PVT_Data.month;
        Packet.Day = UBlox.PVT_Data.day;
        Packet.Hour = UBlox.PVT_Data.hour;
        Packet.Minute = UBlox.PVT_Data.min;
        Packet.Second = UBlox.PVT_Data.sec;
        Packet.Pressure = PressureSensor.SensorData.FilteredP;
        Packet.Temperature = (int16_t)Thermistor.Temperature;
        Packet.Humidity = HumiditySensor.CorrectedHumidity;
        Packet.HumidityTemperature = HumiditySensor.CorrectedTemperature;
        Packet.Latitude = UBlox.PVT_Data.lat;
        Packet.Longitude = UBlox.PVT_Data.lon;
        Packet.hMSL = UBlox.PVT_Data.hMSL;
        Packet.Satellites = UBlox.PVT_Data.numSV;

        // Save to memory if configured so
        if (Spansion.Flag_MemoryFull == RESET)
        {
          switch (FlashMode)
          {
            case FLASH_MODE_DISABLE:
              break;

            case FLASH_MODE_GPS:
              if (UBlox.Flag_Valid == SET)
              {
                Spansion_SaveXQPacket(&Spansion, &Packet);
              }
              break;

            case FLASH_MODE_ON:
              Spansion_SaveXQPacket(&Spansion, &Packet);
              break;
          } // switch
        } // if (Spansion.Flag_MemoryFull == RESET)
      } // if (PacketCounter == iMetXQ_Config.DataReportRate)
    } // if (GetDRRFlagStatus() == SET)
  } // while
} // main

static void InitializeSensors(void)
{
  // Initialize Pressure Sensor
  MS5607_Init(&PressureSensor);

  //Wait(PressureSensor.WaitChannel, 50);
  //while (GetWaitFlagStatus(PressureSensor.WaitChannel == SET));

  // Initialize External Thermistor
  Thermistor.ADC.I2Cx = I2C1;
  Thermistor.ADC.Address = ADS1115_Address_GND;
  Thermistor_Init(&Thermistor);

  //Wait(Thermistor.WaitChannel, 50);
  //while (GetWaitFlagStatus(Thermistor.WaitChannel == SET));

  // Initialize Humidity Sensor
  HYT271_Init(&HumiditySensor);
  //Wait(HumiditySensor.WaitChannel, 50);
  //while (GetWaitFlagStatus(HumiditySensor.WaitChannel == SET));

  // Initialize GPS
  UBlox.I2Cx = I2C1;
  UBlox_Init(&UBlox);
  // Start the state machine if ready
  if (UBlox.State != UBLOX_ST_OFFLINE)
  {
    UBlox.State = UBLOX_ST_START;
  }
} // InitializeSensors

#endif
