#ifndef __CONFIG_H
#define __CONFIG_H
// **************************************************************************
//
//      InterMet Africa (Pty) Ltd
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Andrew N. Spencer
//
//                  ANSWER SYSTEMS
//                  P.O. Box 38679
//                  PINELANDS 7430
//
//                  Ph : (021) 532-2351
//                  Fx : (021) 532-2351
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   Cfg.h
//
//      CONTENTS    :   Header file for configuration routines
//
// **************************************************************************

#ifdef __CONFIG_C
#define CFGLOCN
#else
#define CFGLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// Flash Information
#define CFG_PAGE_LOCN               ((uint32_t)0x0800F800)
//#define CFG_PAGE_LOCN               ((uint32_t)0x0800FF00)

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint32_t SerialNumber;
  uint16_t DataReportRate;
  sSteinhartCoefficients Coefficients;
  int16_t HumidityBias;
  int16_t PressureBias;
  uint8_t FlashMode;
  uint32_t DataCRC;
} sConfig;

typedef union
{
  sConfig   Config;
  uint16_t  Data[sizeof(sConfig) / sizeof(uint16_t)];
} uConfig;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
CFGLOCN sConfig iMetXQ_Config;
CFGLOCN sConfig iMetXQ_Default;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
CFGLOCN void Config_Init(void);
CFGLOCN FLASH_Status Config_Save(void);
CFGLOCN void Config_Get(void);

#endif
