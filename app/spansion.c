#ifndef __SPANSION_C
#define __SPANSION_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-XQ Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   spansion.c
//
//      CONTENTS    :   File for Spansion 16MB serial flash memory management
//
// **************************************************************************

// **************************************************************************
//
// INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//        CONSTANTS
// **************************************************************************
#define CMD_WRITE_ENABLE          0x06
#define CMD_WRITE_DISABLE         0x04
#define CMD_READ_STATUS           0x05
#define CMD_WRITE_STATUS          0x01
#define CMD_READ_DATA             0x03
#define CMD_FAST_READ             0x0B
#define CMD_FAST_READ_DUAL        0x3B
#define CMD_PAGE_PROGRAM          0x02
#define CMD_BLOCK_ERASE           0xD8
#define CMD_SECTOR_ERASE          0x20
#define CMD_CHIP_ERASE            0xC7
#define CMD_PWR_DOWN              0xB9
#define CMD_DEV_ID                0x90
#define CMD_JEDEC_ID              0x9F

#define SPI_TX_SIZE               50
#define SPI_RX_SIZE               50

#define SPI_ST_IDLE               0
#define SPI_ST_WRITE              1
#define SPI_ST_READ               2

#define STAT_SRP_BIT              0x80
#define STAT_RSV_BIT              0x40
#define STAT_BP3_BIT              0x20
#define STAT_BP2_BIT              0x10
#define STAT_BP1_BIT              0x08
#define STAT_BP0_BIT              0x04
#define STAT_WEL_BIT              0x02
#define STAT_WIP_BIT              0x01

// *************************************************************************
//        TYPES
// *************************************************************************




// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void InitSPI(void);
static void SPI_Transmit(uint8_t* Data, uint8_t Length);
static void SPI_Receive(uint8_t* Data, uint8_t Length);
static FlagStatus GetWIPFlagStatus(void);
static void WriteEnable(void);
static void WriteDisable(void);
static void WriteByte(sSpansion_16MB* Memory, uint8_t Data);
static uint8_t ReadByte(uint32_t Address);
static void ClearBuffer(sSpansion_Buffer* Buffer);
static void WriteToBuffer(sSpansion_Buffer* Buffer, uint8_t Data);
void SectorErase(uint32_t Address);

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
uint8_t Command;

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the spansion GPIO for write protection and enable
//              hardware lines
//
//  UPDATED   : 2015-04-07 JHM
//
// **************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(MEM_CS_RCC, ENABLE);

  // Memory Enable Output
  GPIO_InitStructure.GPIO_Pin = MEM_CS_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(MEM_CS_PORT, &GPIO_InitStructure);

  // Set chip select high - this disables the chip (must be enabled prior to
  // read/write)
  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);
} // InitGPIO

// **************************************************************************
//
//  FUNCTION  : InitSPI
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes spansion SPI port in master mode
//
//  UPDATED   : 2015-04-07 JHM
//
// **************************************************************************
static void InitSPI(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef SPI_InitStructure;


  // GPIO clocks
  RCC_AHBPeriphClockCmd(MEM_SCK_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(MEM_MOSI_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(MEM_MISO_RCC, ENABLE);

  // Enable SPI clock
  RCC_APB1PeriphClockCmd(MEM_SPI_RCC, ENABLE);

  // Configure SPI Pins
  // Map pins
  GPIO_PinAFConfig(MEM_SCK_PORT, MEM_SCK_PIN_SRC, MEM_SCK_AF);
  GPIO_PinAFConfig(MEM_MOSI_PORT, MEM_MOSI_PIN_SRC, MEM_MOSI_AF);
  GPIO_PinAFConfig(MEM_MISO_PORT, MEM_MISO_PIN_SRC, MEM_MISO_AF);

  // Configure port hardware
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

  GPIO_InitStructure.GPIO_Pin = MEM_SCK_PIN;
  GPIO_Init(MEM_SCK_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = MEM_MOSI_PIN;
  GPIO_Init(MEM_MOSI_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = MEM_MISO_PIN;
  GPIO_Init(MEM_MISO_PORT, &GPIO_InitStructure);

  // SPI peripheral configuration
  SPI_Cmd(MEM_SPI, DISABLE);
  SPI_I2S_DeInit(MEM_SPI);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  // SPI frequency = MCLK / BaudRatePrescaler = 48MHZ / 2 = 24 MHZ
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;

  SPI_Init(MEM_SPI, &SPI_InitStructure);

  SPI_RxFIFOThresholdConfig(MEM_SPI, SPI_RxFIFOThreshold_QF);

  // Enable the SPI interface
  SPI_Cmd(MEM_SPI, ENABLE);
} // InitSPI

// **************************************************************************
//
//  FUNCTION  : SPI_Transmit
//
//  I/P       : uint8_t* Data - Pointer to the data array to send
//              uint8_t Length - Length of the message
//
//  O/P       : None.
//
//  OPERATION : Transmits data out the MOSI SPI port
//
//  UPDATED   : 2015-04-09 JHM
//
// **************************************************************************
static void SPI_Transmit(uint8_t* Data, uint8_t Length)
{
  int i;

  // Wait until TX FIFO is empty
  while (SPI_GetTransmissionFIFOStatus(MEM_SPI) != SPI_TransmissionFIFOStatus_Empty);
  // Wait for busy flag to clear
  while (SPI_I2S_GetFlagStatus(MEM_SPI, SPI_I2S_FLAG_BSY) == SET);

  for (i = 0; i < Length; i++)
  {
    SPI_SendData8(MEM_SPI, Data[i]);
    // Wait until TX FIFO is empty
    while (SPI_GetTransmissionFIFOStatus(MEM_SPI) != SPI_TransmissionFIFOStatus_Empty);
    // Wait for busy flag to clear
    while (SPI_I2S_GetFlagStatus(MEM_SPI, SPI_I2S_FLAG_BSY) == SET);
    // Clear the receive buffer since SPI does TX and RX simultaneously
    SPI_ReceiveData8(MEM_SPI);
  }
} // SPI_Transmit

// **************************************************************************
//
//  FUNCTION  : SPI_Receive
//
//  I/P       : uint8_t* Data - Pointer to the data array to send
//              uint8_t Length - Length of the message
//
//  O/P       : None.
//
//  OPERATION : Transmits data out the MOSI SPI port
//
//  UPDATED   : 2015-04-09 JHM
//
// **************************************************************************
static void SPI_Receive(uint8_t* Data, uint8_t Length)
{
  int i;

  for (i = 0; i < Length; i++)
  {
    SPI_I2S_ClearFlag(MEM_SPI, SPI_I2S_FLAG_RXNE);
    // Send null message since SPI does TX and RX simultaneously
    SPI_SendData8(MEM_SPI, 0x00);
    // Enable the transmit empty interrupt (begin transmission)
    while (SPI_I2S_GetFlagStatus(MEM_SPI, SPI_I2S_FLAG_RXNE) == RESET);
    Data[i] = SPI_ReceiveData8(MEM_SPI);
  }
} // SPI_Receive

// **************************************************************************
//
//  FUNCTION  : GetStatus
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2015-04-09 JHM
//
// **************************************************************************
static FlagStatus GetWIPFlagStatus(void)
{
  uint8_t Transmission[1];
  uint8_t Reception[1];

  Transmission[0] = CMD_READ_STATUS;

  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);
  SPI_Transmit(Transmission, 1);
  SPI_Receive(Reception, sizeof(Reception));
  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Write in Progress
  if (Reception[0] & STAT_WIP_BIT)
  {
    return SET;
  }
  else
  {
    return RESET;
  }
} // GetStatus

// **************************************************************************
//
//  FUNCTION  : WriteEnable
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Activates the write enable. This function must be performed
//              before any write, program, or erase command.
//
//  UPDATED   : 2015-04-10 JHM
//
// **************************************************************************
static void WriteEnable(void)
{
  // Set Command (private variable)
  Command = CMD_WRITE_ENABLE;

  // Enable CS
  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Transmit the command
  SPI_Transmit(&Command, 1);

  // Disable CS (Required)
  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);
} // WriteEnable

// **************************************************************************
//
//  FUNCTION  : WriteDisable
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Disables the writing capabilities.
//
//  UPDATED   : 2015-04-10 JHM
//
// **************************************************************************
static void WriteDisable(void)
{
  // Set Command (private variable)
  Command = CMD_WRITE_DISABLE;

  // Enable CS
  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Transmit the command
  SPI_Transmit(&Command, 1);

  // Disable CS (Required)
  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);
} // WriteEnable

// **************************************************************************
//
//  FUNCTION  : WriteByte
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Writes a single byte to a single address in memory
//
//  UPDATED   : 2015-04-10 JHM
//
// **************************************************************************
static void WriteByte(sSpansion_16MB* Memory, uint8_t Data)
{
  uint8_t Command[5];

  // Program command, address, byte
  Command[0] = CMD_PAGE_PROGRAM;
  Command[1] = (uint8_t)((Memory->Index >> 16) & 0xFF);
  Command[2] = (uint8_t)((Memory->Index >> 8) & 0xFF);
  Command[3] = (uint8_t)(Memory->Index & 0xFF);
  Command[4] = Data;

  WriteEnable();

  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);

  SPI_Transmit(Command, sizeof(Command));

  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);

  WriteDisable();

  // Increment Index
  if (Memory->Index == MEM_SIZE_BYTES)
  {
    Memory->Flag_MemoryFull = SET;
    return;
  }
  else
  {
    Memory->Index++;
  }
} // WriteByte

// **************************************************************************
//
//  FUNCTION  : ReadByte
//
//  I/P       : uint32_t Address - (0x000000 - 0x1FFFFF)
//
//  O/P       : uint8 - byte
//
//  OPERATION : Reads a single byte from a single address in memory
//
//  UPDATED   : 2015-04-10 JHM
//
// **************************************************************************
static uint8_t ReadByte(uint32_t Address)
{
  uint8_t Command[4];
  uint8_t Byte[1];

  Command[0] = CMD_READ_DATA;
  Command[1] = (uint8_t)((Address >> 16) & 0xFF);
  Command[2] = (uint8_t)((Address >> 8) & 0xFF);
  Command[3] = (uint8_t)(Address & 0xFF);

  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);

  SPI_Transmit(Command, sizeof(Command));
  SPI_Receive(Byte, sizeof(Byte));

  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);

  return Byte[0];
} // ReadByte

// **************************************************************************
//
//  FUNCTION  : ClearBuffer
//
//  I/P       : sSpansion_16MB* Buffer - Pointer to memory buffer structure
//
//  O/P       : None.
//
//  OPERATION : Clear the memory buffer in the memory data structure
//
//  UPDATED   : 2015-08-27 JHM
//
// **************************************************************************
static void ClearBuffer(sSpansion_Buffer* Buffer)
{
  int i;

  // Clear the data array
  for (i = 0; i < MEM_BUF_SIZE; i++)
  {
    Buffer->Data[i] = 0;
  }

  // Reset indexes
  Buffer->Start = 0;
  Buffer->End = 0;
} // ClearBuffer

// **************************************************************************
//
//  FUNCTION  : WriteToBuffer
//
//  I/P       : sSpansion_Buffer* Buffer - Pointer to buffer data structure
//              uint8_t Data - Data byte to be written
//
//  O/P       : None.
//
//  OPERATION : Writes a new data byte to the buffer and increments the
//              index.
//
//  UPDATED   : 2015-09-23 JHM
//
// **************************************************************************
static void WriteToBuffer(sSpansion_Buffer* Buffer, uint8_t Data)
{
  // Add the data into the buffer
  Buffer->Data[Buffer->End] = Data;

  // Increment the buffer (safe)
  Buffer->End = (Buffer->End + 1) % MEM_BUF_SIZE;
} // WriteToBuffer

// **************************************************************************
//
//  FUNCTION  : SectorErase
//
//  I/P       : uint32_t Address - (0x000000 - 0x1FFFFF)
//
//  O/P       : None
//
//  OPERATION : Erases a 4 kB sector of memory.  This routine takes
//              approximately 700 us to run
//
//  UPDATED   : 2015-05-21 JHM
//
// **************************************************************************
void SectorErase(uint32_t Address)
{
  uint8_t Command[4];

  Command[0] = CMD_SECTOR_ERASE;
  Command[1] = (uint8_t)((Address >> 16) & 0xFF);
  Command[2] = (uint8_t)((Address >> 8) & 0xFF);
  Command[3] = (uint8_t)(Address & 0xFF);

  // Enable writing (required)
  WriteEnable();

  // Enable CS (active low)
  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Send command
  SPI_Transmit(Command, sizeof(Command));

  // Disable CS
  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Disable writing
  WriteDisable();

  while (GetWIPFlagStatus() == SET);
} // ReadByte

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : Spansion_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the spansion memory peripherals
//
//  UPDATED   : 2015-04-07 JHM
//
// **************************************************************************
void Spansion_Init(sSpansion_16MB* Memory)
{
  uint32_t Index;

  // Initialize data structure
  Memory->Index = 0;
  Memory->Flag_MemoryFull = RESET;
  Memory->Flag_WIP = RESET;
  Memory->State = MEM_ST_IDLE;

  // Initialize flash mode
  FlashMode = iMetXQ_Config.FlashMode;
  PacketCounter = 0;

  // Initialize the enable and write protection hardware lines
  InitGPIO();

  // Initialize the SPI peripheral
  InitSPI();

  // Initialize the index
  Index = 0;

  // Clear the memory buffer
  ClearBuffer(&Memory->Buffer);

  // Start to cycle through the memory looking for available space
  while (Index < (MEM_SIZE_BYTES - MEM_XQ_SIZE))
  {
    // Memory is full
    if (Index >= (MEM_SIZE_BYTES - MEM_XQ_SIZE))
    {
      Memory->Flag_MemoryFull = SET;
      Memory->Index = 0;
      break;
    }

    if (ReadByte(Index) == 0xFF)
    {
      break;
    }
    else
    {
      Index += MEM_XQ_SIZE;
    }
  }

  // Set the memory index
  Memory->Index = Index;
} // Spansion_Init

// **************************************************************************
//
//  FUNCTION  : Spansion_Handler
//
//  I/P       : sSpansion_16MB* Memory - Pointer to memory data structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the memory data structure
//
//  UPDATED   : 2015-08-27 JHM
//
// **************************************************************************
void Spansion_Handler(sSpansion_16MB* Memory)
{
  /*
  if (GetWaitFlagStatus(Memory->WaitChannel) == SET)
  {
    return;
  }
  */

  switch (Memory->State)
  {
    case MEM_ST_IDLE:
      if (Memory->Buffer.Start != Memory->Buffer.End)
      {
        Memory->State = MEM_ST_WRITE;
      }
      break;

    case MEM_ST_WRITE:
      // Write the byte from the buffer to memory
      WriteByte(Memory, Memory->Buffer.Data[Memory->Buffer.Start]);
      // Increment buffer index (safe)
      Memory->Buffer.Start = (Memory->Buffer.Start + 1) % MEM_BUF_SIZE;
      // Check if we have transmitted everything in the buffer
      if (Memory->Buffer.Start == Memory->Buffer.End)
      {
        Memory->State = MEM_ST_IDLE;
      }
      break;

    default:
      break;
  } // switch
} // Spansion_Handler

// **************************************************************************
//
//  FUNCTION  : ChipErase
//
//  I/P       : None
//
//  O/P       : None
//
//  OPERATION : Erases the entire contents of the flash memory. This routine
//              can take up to twelve seconds
//
//  UPDATED   : 2015-04-13 JHM
//
// **************************************************************************
void Spansion_ChipErase(sSpansion_16MB* Memory)
{
  uint8_t Command[1];

  Command[0] = CMD_CHIP_ERASE;

  // Enable writing (required)
  WriteEnable();

  // Enable CS (active low)
  GPIO_ResetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Send command
  SPI_Transmit(Command, sizeof(Command));

  // Disable CS
  GPIO_SetBits(MEM_CS_PORT, MEM_CS_PIN);

  // Wait
  while(GetWIPFlagStatus() == SET)
  {
#ifdef WATCHDOG
    // Reset the watchdog - without this the routine will cause reset
    WWDG_SetCounter(WWDG_MAX_CNT);
#endif
  }

  // Reset memory index
  Memory->Index = 0x00000000;

  // Reset memory full flag
  Memory->Flag_MemoryFull = RESET;
} // ChipErase

// **************************************************************************
//
//  FUNCTION  : Spansion_SaveXQPacket
//
//  I/P       : sSpansion_16MB* Memory = Pointer to spansion memory data structure
//              sXQ_Packet* Data = Pointer to new XQ data packet to write
//
//  O/P       : None.
//
//  OPERATION : Saves an XQ data packet to the next location in memory.
//
//  UPDATED   : 12/19/2016 JHM
//
// **************************************************************************
void Spansion_SaveXQPacket(sSpansion_16MB* Memory, sXQ_Packet* Data)
{
  uint8_t Bytes[MEM_XQ_SIZE];
  uint8_t* ptr;
  int i;

  if (Memory->Index >= (MEM_SIZE_BYTES - MEM_XQ_SIZE))
  {
    Memory->Flag_MemoryFull = SET;
    return;
  }

  // Set pointer to the start of the array
  ptr = &Bytes[0];

  // Year
  memcpy(ptr, &Data->Year, sizeof(Data->Year));
  ptr += sizeof(Data->Year);
  // Month
  memcpy(ptr, &Data->Month, sizeof(Data->Month));
  ptr += sizeof(Data->Month);
  // Day
  memcpy(ptr, &Data->Day, sizeof(Data->Day));
  ptr += sizeof(Data->Day);
  // Hour
  memcpy(ptr, &Data->Hour, sizeof(Data->Hour));
  ptr += sizeof(Data->Hour);
  // Minute
  memcpy(ptr, &Data->Minute, sizeof(Data->Minute));
  ptr += sizeof(Data->Minute);
  // Second
  memcpy(ptr, &Data->Second, sizeof(Data->Second));
  ptr += sizeof(Data->Second);
  // Pressure
  memcpy(ptr, &Data->Pressure, sizeof(Data->Pressure));
  ptr += sizeof(Data->Pressure);
  // Temperature
  memcpy(ptr, &Data->Temperature, sizeof(Data->Temperature));
  ptr += sizeof(Data->Temperature);
  // Humidity
  memcpy(ptr, &Data->Humidity, sizeof(Data->Humidity));
  ptr += sizeof(Data->Humidity);
  // Humidity Temperature
  memcpy(ptr, &Data->HumidityTemperature, sizeof(Data->HumidityTemperature));
  ptr += sizeof(Data->HumidityTemperature);
  // Satellites
  memcpy(ptr, &Data->Satellites, sizeof(Data->Satellites));
  ptr += sizeof(Data->Satellites);
  // Longitude
  memcpy(ptr, &Data->Longitude, sizeof(Data->Longitude));
  ptr += sizeof(Data->Longitude);
  // Latitude
  memcpy(ptr, &Data->Latitude, sizeof(Data->Latitude));
  ptr += sizeof(Data->Latitude);
  // MSL Altitude
  memcpy(ptr, &Data->hMSL, sizeof(Data->hMSL));

  // Move the data into the buffer
  for (i = 0; i < MEM_XQ_SIZE; i++)
  {
    WriteToBuffer(&Memory->Buffer, Bytes[i]);
  }
} // Spansion_SaveXQPacket

// **************************************************************************
//
//  FUNCTION  : Spansion_ReadXQPacket
//
//  I/P       : sSpansion_16MB* Memory = Pointer to spansion memory data structure
//              sXQ_Packet* Data = Pointer to new XQ data packet to write
//              uint32_t Location = The destination location in memory
//
//  O/P       : None.
//
//  OPERATION : Reads a data packet into memory at the location specified.
//
//  UPDATED   : 12/19/2016 JHM
//
// **************************************************************************
void Spansion_ReadXQPacket(sSpansion_16MB* Memory, sXQ_Packet* Data, uint32_t Location)
{
  uint8_t Bytes[MEM_XQ_SIZE];
  uint8_t* ptr;
  int i;

  for (i = 0; i < MEM_XQ_SIZE; i++)
  {
    Bytes[i] = ReadByte(Location);
    Location++;
  }

  ptr = &Bytes[0];

  // Year
  memcpy(&Data->Year, ptr , sizeof(Data->Year));
  ptr += sizeof(Data->Year);
  // Month
  memcpy(&Data->Month, ptr, sizeof(Data->Month));
  ptr += sizeof(Data->Month);
  // Day
  memcpy(&Data->Day, ptr, sizeof(Data->Day));
  ptr += sizeof(Data->Day);
  // Hour
  memcpy(&Data->Hour, ptr, sizeof(Data->Hour));
  ptr += sizeof(Data->Hour);
  // Minute
  memcpy(&Data->Minute, ptr, sizeof(Data->Minute));
  ptr += sizeof(Data->Minute);
  // Second
  memcpy(&Data->Second, ptr, sizeof(Data->Second));
  ptr += sizeof(Data->Second);
  // Pressure
  memcpy(&Data->Pressure, ptr, sizeof(Data->Pressure));
  ptr += sizeof(Data->Pressure);
  // Temperature
  memcpy(&Data->Temperature, ptr, sizeof(Data->Temperature));
  ptr += sizeof(Data->Temperature);
  // Humidity
  memcpy(&Data->Humidity, ptr, sizeof(Data->Humidity));
  ptr += sizeof(Data->Humidity);
  // Humidity Temperature
  memcpy(&Data->HumidityTemperature, ptr, sizeof(Data->HumidityTemperature));
  ptr += sizeof(Data->HumidityTemperature);
  // Satellites
  memcpy(&Data->Satellites, ptr, sizeof(Data->Satellites));
  ptr += sizeof(Data->Satellites);
  // Longitude
  memcpy(&Data->Longitude, ptr, sizeof(Data->Longitude));
  ptr += sizeof(Data->Longitude);
  // Latitude
  memcpy(&Data->Latitude, ptr, sizeof(Data->Latitude));
  ptr += sizeof(Data->Latitude);
  // MSL Altitude
  memcpy(&Data->hMSL, ptr, sizeof(Data->hMSL));
} // Spansion_ReadXQPacket

#endif
