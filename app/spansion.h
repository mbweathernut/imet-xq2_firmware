#ifndef __SPANSION_H
#define __SPANSION_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Iridium Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   spansion.h
//
//      CONTENTS    :   Header file for Spansion 16MB serial flash memory
//
// **************************************************************************

#ifdef __SPANSION_C
#define MEMLOCN
#else
#define MEMLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************
// SPI Port Peripheral Configuration
#define MEM_SPI            SPI3
#define MEM_SPI_RCC        RCC_APB1Periph_SPI3

// Interrupt
#define MEM_SPI_IRQ        SPI3_IRQn

// SPI Pin Configuration
#define MEM_SCK_PORT       GPIOA
#define MEM_SCK_RCC        RCC_AHBPeriph_GPIOA
#define MEM_SCK_PIN        GPIO_Pin_1
#define MEM_SCK_PIN_SRC    GPIO_PinSource1
#define MEM_SCK_AF         GPIO_AF_6

#define MEM_MISO_PORT      GPIOA
#define MEM_MISO_RCC       RCC_AHBPeriph_GPIOA
#define MEM_MISO_PIN       GPIO_Pin_2
#define MEM_MISO_PIN_SRC   GPIO_PinSource2
#define MEM_MISO_AF        GPIO_AF_6

#define MEM_MOSI_PORT      GPIOA
#define MEM_MOSI_RCC       RCC_AHBPeriph_GPIOA
#define MEM_MOSI_PIN       GPIO_Pin_3
#define MEM_MOSI_PIN_SRC   GPIO_PinSource3
#define MEM_MOSI_AF        GPIO_AF_6

#define MEM_CS_PORT        GPIOB
#define MEM_CS_PIN         GPIO_Pin_2
#define MEM_CS_RCC         RCC_AHBPeriph_GPIOB

#define MEM_SIZE_BYTES     2097152
#define MEM_SIZE_SECTOR    4096
#define MEM_NUM_SECTORS    512

#define MEM_BUF_SIZE       128

#define MEM_ST_IDLE        0
#define MEM_ST_WRITE       1

#define MEM_XQ_SIZE        30

#define FLASH_MODE_DISABLE 0
#define FLASH_MODE_GPS     1
#define FLASH_MODE_ON      2

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint8_t Data[MEM_BUF_SIZE];
  uint16_t Start;
  uint16_t End;
} sSpansion_Buffer;

typedef struct
{
  uint8_t State;
  //uint8_t WaitChannel;
  FlagStatus Flag_WIP;
  sSpansion_Buffer Buffer;
  uint32_t Index;
  FlagStatus Flag_MemoryFull;
} sSpansion_16MB;

typedef struct
{
  // Time stamp
  uint16_t Year;
  uint8_t Month;
  uint8_t Day;
  uint8_t Hour;
  uint8_t Minute;
  uint8_t Second;
  // Met Data
  int32_t Pressure;
  int16_t Temperature;
  int16_t Humidity;
  int16_t HumidityTemperature;
  // GPS Data
  int32_t Latitude;
  int32_t Longitude;
  int32_t hMSL;
  uint8_t Satellites;
} sXQ_Packet;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
MEMLOCN sSpansion_16MB Spansion;
MEMLOCN sXQ_Packet Packet;
MEMLOCN uint8_t FlashMode;
MEMLOCN uint16_t PacketCounter;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
MEMLOCN void Spansion_Init(sSpansion_16MB* Memory);
MEMLOCN void Spansion_Handler(sSpansion_16MB* Memory);
MEMLOCN void Spansion_ChipErase(sSpansion_16MB* Memory);
MEMLOCN void Spansion_SaveXQPacket(sSpansion_16MB* Memory, sXQ_Packet* Data);
MEMLOCN void Spansion_ReadXQPacket(sSpansion_16MB* Memory, sXQ_Packet* Data, uint32_t Location);

#endif /* __SPANSION_H */
