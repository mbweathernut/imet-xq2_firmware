#ifndef __ADS1115_C
#define __ADS1115_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   ADS1115.c
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      ADS1115 analog-to-digital converter
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************

#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
#define  REG_CONV         0x00
#define  REG_CFG          0x01
#define  REG_TLO          0x02
#define  REG_THI          0x03


#define  CFG_COMP_Q0      0x01
#define  CFG_COMP_Q1      0x02
#define  CFG_COMP_LAT     0x04
#define  CFG_COMP_POL     0x08
#define  CFG_COMP_MODE    0x10
#define  CFG_DR0          0x20
#define  CFG_DR1          0x40
#define  CFG_DR2          0x80
#define  CFG_MODE         0x100
#define  CFG_PGA0         0x200
#define  CFG_PGA1         0x400
#define  CFG_PGA2         0x800
#define  CFG_MUX0         0x1000
#define  CFG_MUX1         0x2000
#define  CFG_MUX2         0x4000
#define  CFG_OS           0x8000

#define  EXP2_15          32768

#define  ADC_I2C_TIMEOUT  5000
// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************


// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void SetConfig(sADS1115* ADC_Structure, uint16_t Value);
static uint16_t GetConfig(sADS1115* ADC_Structure);
static void StartConversion(sADS1115* ADC_Structure);
static int16_t GetConversion(sADS1115* ADC_Structure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : SetConfig
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//              uint16_t Value - The value the configuration register will
//                               be set to
//
//  O/P       : None.
//
//  OPERATION : Loads a new configuration value into the config register
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
static void SetConfig(sADS1115* ADC_Structure, uint16_t Value)
{
  int i;
  uint8_t Bytes[3];
  uint16_t Timeout;

  Bytes[0] = REG_CFG;
  Bytes[1] = (uint8_t)(Value >> 8);
  Bytes[2] = (uint8_t)(Value & 0xFF);

  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_ICR_NACKCF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 3, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  for ( i = 0; i < 3; i++)
  {
    Timeout = 500;
    while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
           (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
            Timeout){Timeout--;}

    if ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
    {
      ADC_Structure->Flag_CommsFault = SET;
      return;
    }
    else
    {
      // Send the data
      I2C_SendData(ADC_Structure->I2Cx, Bytes[i]);
    }
  }

  while (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TC) == SET);
} // SetConfig

// **************************************************************************
//
//  FUNCTION  : SetConfig
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : uint16_t - The value of the configuration register
//
//  OPERATION : Reads the value of the config register
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
static uint16_t GetConfig(sADS1115* ADC_Structure)
{
  uint16_t Value;
  uint8_t Byte;
  uint32_t Timeout;

  // Initialize Values
  Value = 0;

  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
         (Timeout)){ Timeout--;}

  // Check for NACK flag
  if ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
  {
    I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
    ADC_Structure->Flag_CommsFault = SET;

    return 0;
  }
  else
  {
    // Send the data
    I2C_SendData(ADC_Structure->I2Cx, REG_CFG);
  }
  // Wait for data to send
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  Timeout = ADC_I2C_TIMEOUT;
  // Wait for data to become available
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) && (Timeout)){Timeout--;};
  if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;

    return 0;
  }
  Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
  Value = (uint16_t)(Byte << 8);

  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) && (Timeout)){Timeout--;};
  if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;

    return 0;
  }
  Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
  Value |= (uint16_t)Byte;

  Timeout = ADC_I2C_TIMEOUT;
  // Wait for transmission to complete
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}

  return Value;
} // SetConfig

// **************************************************************************
//
//  FUNCTION  : StartConversion
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Immediately starts a conversion. This routine assumes the
//              ADC has already been configured correctly for VCC or NTC
//
//  UPDATED   : 2015-06-14 JHM
//
// **************************************************************************
static void StartConversion(sADS1115* ADC_Structure)
{
  uint16_t RegValue;

  RegValue = GetConfig(ADC_Structure);

  // Escape if a there was an error reading the cfg register
  if (ADC_Structure->Flag_CommsFault == SET)
  {
    return;
  }

  // Proceed if there is not already a conversion in progress
  if (RegValue & CFG_OS)
  {
    // Set the OS bit and leave the other values the same
    SetConfig(ADC_Structure, RegValue | CFG_OS);
  }
  else
  {
    ADC_Structure->Flag_CommsFault = SET;
  }
} // StartConversion

// **************************************************************************
//
//  FUNCTION  : GetConversion
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Immediately starts a conversion. This routine assumes the
//              ADC has already been configured correctly for VCC or NTC
//
//  UPDATED   : 2015-06-14 JHM
//
// **************************************************************************
static int16_t GetConversion(sADS1115* ADC_Structure)
{
  int16_t Value;
  uint8_t Byte;
  uint16_t Timeout;

  // Ensure we do not send a configuration during a conversion in progress.
  // This loop should never be entered.
  while (ADS1115_GetConvStatus(ADC_Structure) == SET)
  {
    DelayMs(1);
  }

  // Clear the flags we are using
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  // Wait for ACK/NACK
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
         Timeout){Timeout--;}

  if (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    ADC_Structure->Flag_CommsFault = SET;
    return 0;
  }
  else if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;
    return 0;
  }
  else
  {
    // Write the pointer register value
    I2C_SendData(ADC_Structure->I2Cx, REG_CONV);
  }
  // Wait for transmission to complete
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  // Wait for data to become available
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_NACKF) == RESET) &&
         (Timeout)){Timeout--;}

  if (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET || (Timeout == 0))
  {
    I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
    ADC_Structure->Flag_CommsFault = SET;

    return 0;
  }
  else
  {
    Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
    Value = (uint16_t)(Byte << 8);
  }

  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_NACKF) == RESET) &&
         (Timeout)){Timeout--;}
  if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;

    return 0;
  }

  Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
  Value |= (uint16_t)Byte;

  // Wait for transmission to complete
  Timeout = ADC_I2C_TIMEOUT;
  while (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET){Timeout--;}
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  return Value;
} // GetConversion

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : ADS1115_Init
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the structure for the ADS1115. Tests communication
//              with the module. If there is an error or the sensor is offline,
//              the Flag_CommsFault flag will be SET.
//
//  UPDATED   : 2015-06-15 JHM
//
// **************************************************************************
void ADS1115_Init(sADS1115* ADC_Structure)
{
  ADC_Structure->Flag_CommsFault = RESET;

  // Get the configuration - this is really just a comms test. If there is a
  // problem the Flag_CommsFault will be set.
  GetConfig(ADC_Structure);

  if (ADC_Structure->Flag_CommsFault == SET)
  {
    return;
  }
} // ADC_Init

// **************************************************************************
//
//  FUNCTION  : ADS1115_SetConfig
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Sends the command to set the configuration of the structure
//              to the values of the ADC_Structure->Config structure.
//
//  UPDATED   : 2015-09-22 JHM
//
// **************************************************************************
void ADS1115_SetConfig(sADS1115* ADC_Structure)
{
  uint16_t SetValue;

  // Build configuration
  SetValue = 0;

  // Disable Comparator
  SetValue |= (CFG_COMP_Q0 + CFG_COMP_Q1);

  // Skip Bits 2 - 4 because they involve the comparator (which is disabled)

  // Set the Mux Configuration
  if (ADC_Structure->Config->Type == ADS1115_MeasType_SingleEnded)
  {
    SetValue |= CFG_MUX2;

    switch(ADC_Structure->Config->Input)
    {
      case ADS1115_Input_AIN0:
        break;
      case ADS1115_Input_AIN1:
        SetValue |= CFG_MUX0;
        break;
      case ADS1115_Input_AIN2:
        SetValue |= CFG_MUX1;
        break;
      case ADS1115_Input_AIN3:
        SetValue |= (CFG_MUX1 + CFG_MUX0);
        break;
    } // switch
  } // if
  else
  {
    // Leave as default setting
    // AINP = AIN0 and AINN = AIN1
  }

  // Set the PGA Configuration
  switch (ADC_Structure->Config->FS)
  {
    case ADS1115_FS_6144:
      break;
    case ADS1115_FS_4096:
      SetValue |= CFG_PGA0;
      break;
    case ADS1115_FS_2048:
      SetValue |= CFG_PGA1;
      break;
    case ADS1115_FS_1024:
      SetValue |= (CFG_PGA1 + CFG_PGA0);
      break;
    case ADS1115_FS_0512:
      SetValue |= CFG_PGA2;
      break;
    case ADS1115_FS_0256:
      SetValue |= (CFG_PGA2 + CFG_PGA0);
      break;
  } // switch

  // Set the Data Rate
  switch (ADC_Structure->Config->DataRate)
  {
    case ADS1115_DataRate_8SPS:
      break;
    case ADS1115_DataRate_16SPS:
      SetValue |= CFG_DR0;
      break;
    case ADS1115_DataRate_32SPS:
      SetValue |= CFG_DR1;
      break;
    case ADS1115_DataRate_64SPS:
      SetValue |= (CFG_DR1 + CFG_DR2);
      break;
    case ADS1115_DataRate_128SPS:
      SetValue |= CFG_DR2;
      break;
    case ADS1115_DataRate_250SPS:
      SetValue |= (CFG_DR2 + CFG_DR0);
      break;
    case ADS1115_DataRate_475SPS:
      SetValue |= (CFG_DR2 + CFG_DR1);
      break;
    case ADS1115_DataRate_860SPS:
      SetValue |= (CFG_DR2 + CFG_DR1 + CFG_DR0);
      break;
  }  // switch

  // Device operating mode power-down single shot (default)
  SetValue |= CFG_MODE;

  // Do not start the conversion
  //SetValue |= CFG_OS;

  SetConfig(ADC_Structure, SetValue);
}  //ADS1115_SetConfig

// **************************************************************************
//
//  FUNCTION  : ADS1115_GetConvStatus
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : SET = Conversion is in progress
//              RESET = No conversion in progress
//
//  OPERATION : Returns the conversion status of the ADC. This reads the
//              configuration register OS bit. Returns SET if a conversion
//              is in progress.
//
//  UPDATED   : 15/05/19 JHM
//
// **************************************************************************
FlagStatus ADS1115_GetConvStatus(sADS1115* ADC_Structure)
{
  uint16_t RegValue;
  FlagStatus Status;

  // Initialize
  Status = SET;

  // Get the configuration register value
  RegValue = GetConfig(ADC_Structure);

  if (ADC_Structure->Flag_CommsFault == SET)
  {
    Status = RESET;
  }

  else if (RegValue & CFG_OS)
  {
    Status = RESET;
  }
  else
  {
    Status = SET;
  }

  return Status;
} // ADS1115_GetConvStatus

// **************************************************************************
//
//  FUNCTION  : ADS1115_StartConversion
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Immediately starts a single-ended conversion of VCC with the
//              PGA set to 2/3.
//
//  UPDATED   : 15/06/18 JHM
//
// **************************************************************************
ADCLOCN void ADS1115_StartConversion(sADS1115* ADC_Structure)
{
  uint16_t CfgValue;

  // Build configuration
  CfgValue = 0;

  // Disable Comparator
  CfgValue |= (CFG_COMP_Q0 + CFG_COMP_Q1);

  // Skip Bits 2 - 4 because they involve the comparator (which is disabled)

  // Set the Mux Configuration
  if (ADC_Structure->Config->Type == ADS1115_MeasType_SingleEnded)
  {
    CfgValue |= CFG_MUX2;

    switch(ADC_Structure->Config->Input)
    {
      case ADS1115_Input_AIN0:
        break;
      case ADS1115_Input_AIN1:
        CfgValue |= CFG_MUX0;
        break;
      case ADS1115_Input_AIN2:
        CfgValue |= CFG_MUX1;
        break;
      case ADS1115_Input_AIN3:
        CfgValue |= (CFG_MUX1 + CFG_MUX0);
        break;
    } // switch
  } // if
  else
  {
    // Leave as default setting
    // AINP = AIN0 and AINN = AIN1
  }

  // Set the PGA Configuration
  switch (ADC_Structure->Config->FS)
  {
    case ADS1115_FS_6144:
      break;
    case ADS1115_FS_4096:
      CfgValue |= CFG_PGA0;
      break;
    case ADS1115_FS_2048:
      CfgValue |= CFG_PGA1;
      break;
    case ADS1115_FS_1024:
      CfgValue |= (CFG_PGA1 + CFG_PGA0);
      break;
    case ADS1115_FS_0512:
      CfgValue |= CFG_PGA2;
      break;
    case ADS1115_FS_0256:
      CfgValue |= (CFG_PGA2 + CFG_PGA0);
      break;
  } // switch

  // Set the Data Rate
  switch (ADC_Structure->Config->DataRate)
  {
    case ADS1115_DataRate_8SPS:
      break;
    case ADS1115_DataRate_16SPS:
      CfgValue |= CFG_DR0;
      break;
    case ADS1115_DataRate_32SPS:
      CfgValue |= CFG_DR1;
      break;
    case ADS1115_DataRate_64SPS:
      CfgValue |= (CFG_DR1 + CFG_DR2);
      break;
    case ADS1115_DataRate_128SPS:
      CfgValue |= CFG_DR2;
      break;
    case ADS1115_DataRate_250SPS:
      CfgValue |= (CFG_DR2 + CFG_DR0);
      break;
    case ADS1115_DataRate_475SPS:
      CfgValue |= (CFG_DR2 + CFG_DR1);
      break;
    case ADS1115_DataRate_860SPS:
      CfgValue |= (CFG_DR2 + CFG_DR1 + CFG_DR0);
      break;
  }  // switch

  // Device operating mode power-down single shot (default)
  CfgValue |= CFG_MODE;

  // Send the configuration to the device
  SetConfig(ADC_Structure, CfgValue);

  // Verify that the configuation is correct
  CfgValue = GetConfig(ADC_Structure);

  if ((ADC_Structure->Flag_CommsFault == SET))
  {
    return;
  }

  // Start the conversion
  StartConversion(ADC_Structure);
} // ADS1115_StartConversion

// **************************************************************************
//
//  FUNCTION  : ADS1115_GetVoltage
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : float - the measured voltage in Volts
//
//  OPERATION : Gets the configuration value and uses this to calculate the
//              voltage.
//
//  UPDATED   : 15/05/19 JHM
//
// **************************************************************************
float ADS1115_GetVoltage(sADS1115* ADC_Structure)
{
  int16_t Conversion;
  float FullScale;

  // Get the single-ended voltage count
  Conversion = GetConversion(ADC_Structure);

  if (ADC_Structure->Flag_CommsFault == SET)
  {
    return -999.9;
  }

  switch (ADC_Structure->Config->FS)
  {
    case ADS1115_FS_6144:
      FullScale = 6.144;
      break;
    case ADS1115_FS_4096:
      FullScale = 4.096;
      break;
    case ADS1115_FS_2048:
      FullScale = 2.048;
      break;
    case ADS1115_FS_1024:
      FullScale = 1.024;
      break;
    case ADS1115_FS_0512:
      FullScale = 0.512;
      break;
    case ADS1115_FS_0256:
      FullScale = 0.256;
      break;
  }

  // Calculate the voltage
  return FullScale * (float)Conversion / (float)EXP2_15;
} // ADS1115_GetVCC

#endif


