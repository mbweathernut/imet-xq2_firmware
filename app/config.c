#ifndef __CONFIG_C
#define __CONFIG_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// ***************************************************************************
//
//      MODULE      :   config.c
//
//      CONTENTS    :   Routines for getting and setting the configuration.
//
// ***************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//       PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//       PRIVATE TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//       PRIVATE VARIABLES
//
// **************************************************************************
// None.


// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// None.


// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION    : InitConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Initializes the configuration structure
//
//  UPDATED     : 2014-06-11 JHM
//
// **************************************************************************
void Config_Init(void)
{
  // Set defaults
  iMetXQ_Default.SerialNumber = 0;
  iMetXQ_Default.DataReportRate = 1; // 1 sec
  iMetXQ_Default.FlashMode = FLASH_MODE_GPS;
  iMetXQ_Default.Coefficients.A0 = STEINHART_A0;
  iMetXQ_Default.Coefficients.A1 = STEINHART_A1;
  iMetXQ_Default.Coefficients.A2 = STEINHART_A2;
  iMetXQ_Default.Coefficients.A3 = STEINHART_A3;
  iMetXQ_Default.Coefficients.BiasT = 0;
  iMetXQ_Default.HumidityBias = 0;
  iMetXQ_Default.PressureBias = 0;

  Config_Get();
} // InitConfig

// **************************************************************************
//
//  FUNCTION    : SaveConfig
//
//  I/P         : None.
//
//  O/P         : FLASH_Status
//                FLASH_COMPLETE = Saved successfully
//                Anything else = Unsuccessful
//
//  OPERATION   : Saves the sonde configuration to flash
//
//  UPDATED     : 2014-06-11 JHM
//
// **************************************************************************
FLASH_Status Config_Save(void)
{
  uint16_t Offset;
  FLASH_Status fsResult = FLASH_COMPLETE;
  uConfig Union;

  // Calculate new CRC Value (excluding checksum value)
  CRC_DeInit();
  iMetXQ_Config.DataCRC = CRC_CalcBlockCRC((uint32_t *)&iMetXQ_Config, (sizeof(iMetXQ_Config)) >> sizeof(iMetXQ_Config.DataCRC));

  // Copy sonde configuration to union structure
  memcpy(&Union, &iMetXQ_Config, sizeof(Union));

  // Unlock the Flash Program Erase controller for writing
  FLASH_Unlock();

  // Clear all pending flags
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);

  // Erase the page
  fsResult = FLASH_ErasePage(CFG_PAGE_LOCN);

  // Program the page with the new values
  Offset = 0;
  while ((Offset < sizeof(sConfig) / sizeof(uint16_t)) &&
         (fsResult == FLASH_COMPLETE))
  {
    fsResult = FLASH_ProgramHalfWord(CFG_PAGE_LOCN + 2 * Offset, Union.Data[Offset]);
    Offset++;
  } // while

  // Lock the flash for safety
  FLASH_Lock();

  return(fsResult);
} // SaveConfig

// **************************************************************************
//
//  FUNCTION    : GetConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Gets the sonde configuration from flash or loads the
//                default values
//
//  UPDATED     : 2014-06-26 JHM
//
// **************************************************************************
void Config_Get(void)
{
  uint16_t Offset;
  uint32_t CalculatedCRC;
  uConfig Union;

  // Read values from flash
  for (Offset = 0; Offset < sizeof(sConfig) / 2; Offset++)
  {
    Union.Data[Offset] = *(uint16_t*)(CFG_PAGE_LOCN + (uint32_t)Offset * 2);
  }

  // Calculate the expected CRC on this data, with the CRC generator set to
  // 32-bit operation with initial=0xFFFFFFFF, polynomial=0x04C11DB7 and no reversing.
  CRC_DeInit();
  CalculatedCRC = CRC_CalcBlockCRC((uint32_t *)&Union,(sizeof(Union) >> sizeof(Union.Config.DataCRC)));

  if (CalculatedCRC != Union.Config.DataCRC)
  {
    // Copy the default settings to the configuration structure
    memcpy(&iMetXQ_Config, &iMetXQ_Default, sizeof(sConfig));
  }
  else
  {
    // Copy the saved settings to the configuration structure
    memcpy(&iMetXQ_Config, &Union, sizeof(sConfig));
  }
} // GetConfig

#endif
